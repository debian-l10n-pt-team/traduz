# Portuguese translation for clamav-data debconf messages
# This file is distributed under the same license as the clamav-data package.
# Ricardo Silva <ardoric@gmail.com>, 2006
# 
msgid ""
msgstr ""
"Project-Id-Version: clamav-data 20060725.011200.1618\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2006-07-26 05:01-0600\n"
"PO-Revision-Date: 2006-08-02 12:22+0100\n"
"Last-Translator: Ricardo Silva <ardoric@gmail.com>\n"
"Language-Team: Native Portuguese <traduz@debianpt.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Generate daily warning e-mails on outdated data files?"
msgstr "Gerar diariamente emails de aviso sobre ficheiros de dados "
"desactualizados?"

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"This package contains a _static_ database of virus patterns for clamav. "
"Since this can be leading to a false sense of security, the package can "
"generate warnings out of cron.daily if the static databases are older than "
"two months."
msgstr ""
"Este pacote contém a base de dados _estática_ dos padrões de vírus para o "
"Clamav. Como isto pode levar a uma falsa sensação de segurança, o pacote pode "
"gerar falsos avisos na tarefa agendada diária cron.daily se as bases de dados "
"forem mais antigas que dois meses."

