# Portuguese translation of mailman-suite debconf template
# Copyright (C) 2018 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the mailman-suite package.
#
# Rui Branco - DebianPT <ruipb@debianpt.org>, 2018.
msgid ""
msgstr ""
"Project-Id-Version: mailman-suite\n"
"Report-Msgid-Bugs-To: mailman-suite@packages.debian.org\n"
"POT-Creation-Date: 2018-03-23 16:50+0100\n"
"PO-Revision-Date: 2018-05-13 10:51+0000\n"
"Last-Translator: Rui Branco - DebianPT <ruipb@debianpt.org>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: string
#. Description
#: ../templates:1001
msgid "Domain name for Mailman3 web interface:"
msgstr "Nome de domínio para a interface web do Mailman3:"

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"The Django project needs a default site domain set. This domain will be used "
"by the webinterface and in mails sent in its name."
msgstr ""
"O projecto Django necessita de um site com domínio definido. Este domínio "
"será usado pela interface web e para envio de emails em seu nome."

#. Type: string
#. Description
#: ../templates:2001
msgid "Domain name for sender email addresses:"
msgstr "Nome de domínio para os endereços de envio de emails: "

#. Type: string
#. Description
#: ../templates:2001
msgid ""
"If the Mailman3 web interface sends emails, this domain will be used for the "
"sender addresses. In particular, it will be 'postorius@<domain>' for "
"internal authentication and 'root@<domain>' for error messages."
msgstr ""
"Se a interface web Mailman3 enviar emails, este domínio será usado para "
"os endereços de envio. Em particular, serão 'postorius@<domain>' para "
"autenticação interna e 'root@<domain>' para mensagens de erro."

#. Type: string
#. Description
#: ../templates:3001
msgid "Username of the Postorius superuser:"
msgstr "Nome de utilizador do super utilizador Postorius:"

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"This is the username of the Postorius superuser. It will have global "
"administrative access to all mailinglists."
msgstr ""
"Este é o nome de utilizador do super utilizador do Postorius. Terá acesso "
"de administração a todas as 'mailing lists'."

#. Type: string
#. Description
#: ../templates:4001
msgid "Email address of the Postorius superuser:"
msgstr "Endereço de email do super utilizador Postorius:"

#. Type: password
#. Description
#: ../templates:5001
msgid "Password for the Postorius superuser:"
msgstr "Palavra-chave do super utilizador Postorius"

#. Type: password
#. Description
#: ../templates:5001
msgid ""
"If an empty password is given, no superuser will be created at all. It then "
"needs to be created manually."
msgstr ""
"Se for dada uma palavra-chave vazia, não será criado nenhum super "
"utilizador. Terá então que ser criado manualmente."

#. Type: select
#. Choices
#: ../templates:6001
msgid "none"
msgstr "nenhum"

#. Type: select
#. Choices
#: ../templates:6001
msgid "apache2"
msgstr "apache2"

#. Type: select
#. Choices
#: ../templates:6001
msgid "nginx"
msgstr "nginx"

#. Type: select
#. Description
#: ../templates:6002
msgid "Web server(s) to configure automatically:"
msgstr "Servidor(es) Web a configurar automaticamente:"

#. Type: select
#. Description
#: ../templates:6002
msgid ""
"Mailman3-web supports any web server with uwsgi support, however only Apache "
"2 and nginx can be configured automatically."
msgstr ""
"O Mailman3-web suporta qualquer servidor web com suporte uwsgi, no entanto "
"apenas o Apache 2 e o nginx podem ser automaticamente configurados."

#. Type: select
#. Description
#: ../templates:6002
msgid ""
"Please select the web server(s) that should be configured automatically for "
"Mailman3-web."
msgstr ""
"Por favor seleccione o servidor web que deverá ser configurado "
"automaticamente para o Mailman3-web."

#. Type: note
#. Description
#: ../templates:7001
msgid "Configuring Mailman3 in Nginx"
msgstr "A configurar o Mailman3 no Nginx"

#. Type: note
#. Description
#: ../templates:7001
msgid ""
"The Mailman3 Nginx configuration file is a vhost configuration. Hence, it "
"comes with a server name which is set to mailman.example.com. You will have "
"to change it properly."
msgstr ""
"O ficheiro de configuração do nginx para o Mailman3 é uma configuração de"
" vhost. "
"Deste modo vem com um nome de servidor definido para mailman.example.com. "
"Terá que o modificar apropriadamente."

#. Type: note
#. Description
#: ../templates:7001
msgid ""
"Please also note that Mailman3 is configured to expect the web interface at "
"URL subdirectory '/mailman3' per default. But in the nginx file, as the "
"configuration includes a vhost, the expected URL root is '/'."
msgstr ""
"Note também que o Mailman3 está configurado de modo a esperar a "
"interface web no directório URL '/mailman3' por predefinição. No entanto "
"no ficheiro nginx, como a configuração inclui um vhost, a raiz do URL "
"esperado é '/'."

#. Type: note
#. Description
#: ../templates:7001
msgid ""
"For the Nginx vhost configuration (without '/mailman3' subdomain) to work, "
"you will have to edit the URL in 'base-url' at '/etc/mailman3/mailman-"
"hyperkitty.cfg' and in 'MAILMAN_ARCHIVER_FROM' at '/etc/mailman3/mailman-web."
"py' accordingly."
msgstr ""
"Para que a configuração vhost do Nginx (sem subdomínio '/mailman3') "
"funcione, terá que editar o URL em 'base-url' no '/etc/mailman3/mailman-"
"hyperkitty.cfg' e em 'MAILMAN_ARCHIVER_FROM' no '/etc/mailman3/ "
"mailman-web.py' adequadamente."

#. Type: note
#. Description
#: ../templates:7001
msgid "See the comments in '/etc/mailman3/nginx.conf' for details."
msgstr "Veja os comentários em '/etc/mailman3/nginx.conf' para detalhes."

#. Type: boolean
#. Description
#: ../templates:8001
msgid "Should the webserver(s) be restarted now?"
msgstr "Deve o servidor web reiniciar agora?"

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"In order to activate the new configuration, the configured web server(s) "
"have to be restarted."
msgstr ""
"De modo a activar a nova configuração, o servidor web configurado deverá "
"ser reiniciado."


