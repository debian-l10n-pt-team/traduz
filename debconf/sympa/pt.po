# Portuguese translation for sympa's debconf messages.
# Copyright (C) 2007, Miguel Figueiredo
# Released under the same license as the sympa package
# Miguel Figueiredo <elmig@debianpt.org>, 2007, 2008, 2010.

msgid ""
msgstr ""
"Project-Id-Version: sympa\n"
"Report-Msgid-Bugs-To: sympa@packages.debian.org\n"
"POT-Creation-Date: 2010-04-12 15:33+0200\n"
"PO-Revision-Date: 2010-05-25 22:06+0100\n"
"Last-Translator: Miguel Figueiredo <elmig@debianpt.org>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Description
#: ../templates:1001
msgid "Default language for Sympa:"
msgstr "Idioma predefinido para o Sympa:"

#. Type: string
#. Description
#: ../templates:2001
msgid "Sympa hostname:"
msgstr "Nome da máquina do Sympa:"

#. Type: string
#. Description
#: ../templates:2001
msgid ""
"This is the name of the machine or the alias you will use to reach sympa."
msgstr ""
"Este é o nome da máquina ou o alias que irá utilizar para chegar ao sympa."

#. Type: string
#. Description
#. Type: string
#. Description
#: ../templates:2001 ../templates:3001
msgid "Example:"
msgstr "Exemplo:"

#. Type: string
#. Description
#: ../templates:2001
msgid "  listhost.cru.fr"
msgstr "  listhost.cru.fr"

#. Type: string
#. Description
#: ../templates:2001
msgid "  Then, you will send your sympa commands to:"
msgstr "  Depois, você irá enviar os seus comandos do sympa para:"

#. Type: string
#. Description
#: ../templates:2001
msgid "  sympa@listhost.cru.fr"
msgstr "  sympa@listhost.cru.fr"

#. Type: string
#. Description
#: ../templates:3001
msgid "Listmaster email address(es):"
msgstr "Endereço(s) de email do listmasters:"

#. Type: string
#. Description
#: ../templates:3001
msgid ""
"Listmasters are privileged people who administrate mailing lists (mailing "
"list superusers)."
msgstr ""
"Os listmasters são pessoas privilegiadas que administram listas de correio "
"(mailing lists superusers)."

#. Type: string
#. Description
#: ../templates:3001
msgid "Please give listmasters email addresses separated by commas."
msgstr "Por favor introduza os endereços de email dos listmasters separados por vírgulas."

#. Type: string
#. Description
#: ../templates:3001
msgid "  postmaster@cru.fr, root@home.cru.fr"
msgstr "  postmaster@cru.fr, root@home.cru.fr"

#. Type: boolean
#. Description
#: ../templates:4001
msgid "Should lists home and spool directories be removed?"
msgstr "Devem ser removidos os directórios home e spool das listas?"

#. Type: boolean
#. Description
#: ../templates:4001
msgid ""
"The lists home directory (/var/lib/sympa) contains the mailing lists "
"configurations, mailing list archives and S/MIME user certificates (when "
"sympa is configured for using S/MIME encryption and authentication). The "
"spool directory (/var/spool/sympa) contains various queue directories."
msgstr ""
"O directório home das listas (/var/lib/sympa) contém as configurações das "
"mailing lists, arquivos das mailing lists e certificados s/MIME dos "
"utilizadores (quando o sympa for configurado para utilizar encriptação e "
"autenticação S/MIME). O directório de spool (/var/spool/sympa) tem vários "
"directórios queue."

#. Type: boolean
#. Description
#: ../templates:4001
msgid ""
"Note that if those directories are empty, they will be automatically removed."
msgstr ""
"Note que se esses directórios estiverem vazios, serão automaticamente "
"removidos."

#. Type: boolean
#. Description
#: ../templates:4001
msgid ""
"Please choose whether you want to remove lists home and spool directories."
msgstr ""
"Por favor escolha se quer remover os directórios de spool e home das listas."

#. Type: string
#. Description
#: ../templates:5001
msgid "URL to access WWSympa:"
msgstr "URL para aceder ao WWSympa:"

#. Type: select
#. Choices
#: ../templates:6001
msgid "Apache 2"
msgstr "Apache 2"

#. Type: select
#. Choices
#: ../templates:6001
msgid "Other"
msgstr "Outro"

#. Type: select
#. Description
#: ../templates:6002
msgid "Which Web Server(s) are you running?"
msgstr "Que tipo de Servidor(es) Web está a correr?"

#. Type: boolean
#. Description
#: ../templates:7001
msgid "Do you want WWSympa to run with FastCGI?"
msgstr "Quer que o WWSympa corra com FastCGI?"

#. Type: boolean
#. Description
#: ../templates:7001
msgid ""
"FastCGI is an Apache module that makes WWSympa run much faster. This option "
"will be activated only if the `libapache-mod-fastcgi' package is installed "
"on your system. Please first make sure you installed this package. FastCGI "
"is required for using the Sympa SOAP server."
msgstr ""
"O FastCGI é um módulo do Apache que torna o WWWSympa muito mais rápido. Esta "
"opção será activada apenas se o pacote `libapache2-mod-fastcgi' estiver "
"instalado no seu sistema. Por favor primeiro assegure-se que instalou este "
"pacote. O FastCGI é necessário para utilizar o servidor SOAP do Sympa."

#. Type: boolean
#. Description
#: ../templates:8001
msgid "Do you want the sympa SOAP server to be used?"
msgstr "Quer que seja utilizado o servidor SOAP do Sympa?"

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"Sympa SOAP server allows to access a Sympa service from within another "
"program, written in any programming language and on any computer. The SOAP "
"server provides a limited set of high level functions including login, "
"which, lists, subscribe, signoff."
msgstr ""
"O servidor SOAP do Sympa permite aceder a um serviço Sympa a partir de outro "
"programa, escrito em qualquer linguagem de programação e em qualquer "
"computador. O servidor SOAP disponibiliza um conjunto limitado de funções de "
"alto nível incluindo login, which, lists, subscribe, signoff."

#. Type: boolean
#. Description
#: ../templates:8001
msgid ""
"The SOAP server uses libsoap-lite-perl package and a webserver like apache."
msgstr ""
"O servidor SOAP utiliza o pacote libsoap-lite-perl e um servidor web como o "
"apache."

#. Type: boolean
#. Description
#: ../templates:9001
msgid "Do you want the Web server to be restarted after installation?"
msgstr "Deseja que o servidor Web seja reiniciado após a instalação?"

#. Type: boolean
#. Description
#: ../templates:9001
msgid ""
"If you don't want the webserver to be restarted, please make sure that "
"wwsympa and the Sympa SOAP server are not running or the database may "
"contain duplicates."
msgstr ""
"Se não quiser que o servidor Web seja reiniciado, por favor assegure-se que "
"o wwsympa e o servidor SOAP do Sympa não estão a correr ou a base de dados "
"poderá conter duplicados."

#. Type: boolean
#. Description
#: ../templates:10001
msgid "Should the web archives and the bounce directory be removed?"
msgstr "Devem os arquivos web e o directório bounce ser removidos?"

#. Type: boolean
#. Description
#: ../templates:10001
msgid ""
"If you used the default configuration, WWSympa web archives are located in /"
"var/lib/sympa/wwsarchive. The WWSympa bounce directory contains bounces (non-"
"delivery reports) and is set to /var/spool/sympa/wwsbounce by default."
msgstr ""
"Se você utilizou a configuração por omissão, os arquivos web WWSympa estão "
"localizados em /var/lib/sympa/wwsarchive. O directório bounce do WWSympa "
"contém bounces (relatórios não entregues) e está definido por omissão em /"
"var/spool/sympa/wwsbounce."

#. Type: boolean
#. Description
#: ../templates:10001
msgid ""
"Please choose whether you want to remove the web archives and the bounce "
"directory."
msgstr ""
"Por favor escolha se quer remover os arquivos web e o directório bounce."

#~ msgid "Do you want S/MIME authentication and encryption?"
#~ msgstr "Deseja autenticação e encriptação S/MIME?"

#~ msgid ""
#~ "S/MIME allows messages to be encrypted within a given list and also "
#~ "allows users to be authenticated."
#~ msgstr ""
#~ "S/MIME permite que as mensagens sejam encriptadas dentro de uma dada "
#~ "lista e também permite que os utilizadores sejam autenticados."

#~ msgid ""
#~ "This option works only if the `openssl' package is installed on your "
#~ "system. Please first make sure you installed this package."
#~ msgstr ""
#~ "Esta opção apenas funciona se o pacote `openssl' estiver instalado no seu "
#~ "sistema. Por favor, primeiro assegure-se que instalou este pacote."

#~ msgid "What is the password for the lists private keys?"
#~ msgstr "Qual é a password para as listas das chaves privadas?"

#~ msgid "This password does protect the access to lists private keys."
#~ msgstr "Esta password protege o acesso a listas de chaves privadas."

#~ msgid "Please note that you are not allowed to give an empty password."
#~ msgstr "Por favor note que não está autorizado a dar uma password vazia."

#~ msgid "Re-enter the password for the lists private keys for verification:"
#~ msgstr ""
#~ "Introduza novamente a password para as listas de chaves privadas para "
#~ "verificação:"

#~ msgid ""
#~ "Please enter the same password again to verify you have typed it "
#~ "correctly."
#~ msgstr ""
#~ "Por favor introduza, novamente, a mesma password para verificar que você "
#~ "a escreveu correctamente."

#~ msgid "Do you want WWSympa to be installed?"
#~ msgstr "Quer que seja instalado o WWSympa?"

#~ msgid ""
#~ "WWSympa is a powerful Web interface for both administrating Sympa mailing "
#~ "lists and managing user subscriptions."
#~ msgstr ""
#~ "O WWSympa é um poderoso interface Web para administrar quer a mailing "
#~ "list quer as assinaturas dos utilizadores."

#~ msgid ""
#~ "This interface does not run without a RDBMS. So, if you did not configure "
#~ "Sympa for using a database, you will have to reconfigure Sympa."
#~ msgstr ""
#~ "Este interface não corre sem um RDBMS. Por isso, se não configurou o "
#~ "Sympa para utilizar uma base de dados, terá de reconfigurar o Sympa."

#~ msgid ""
#~ "Once installed, you will be able to access WWSympa at the following "
#~ "address"
#~ msgstr ""
#~ "Uma vez instalado, você poderá aceder ao WWSympa no seguinte endereço"

#~ msgid "Do you want to store the subscription information in a database?"
#~ msgstr "Deseja guardar a informação de subscrição numa base de dados?"

#~ msgid ""
#~ "It is possible to store the subscription information in a database "
#~ "instead of a simple text file, making accesses to user information much "
#~ "faster."
#~ msgstr ""
#~ "É possível guardar a informação de subscrição numa base de dados ao invés "
#~ "de um ficheiro de texto simples, tornando os acessos à informação de "
#~ "utilizador muito mais rápido."

#~ msgid ""
#~ "Using a database is also mandatory when you want to use the WWSympa "
#~ "administration interface."
#~ msgstr ""
#~ "Utilizar uma base de dados também é mandatório quando quiser utilizar o "
#~ "interface de administração WWSympa."

#~ msgid "PostgreSQL"
#~ msgstr "PostgreSQL"

#~ msgid "MySQL"
#~ msgstr "MySQL"

#~ msgid "What type of database are you using?"
#~ msgstr "Que tipo de base de dados está a utilizar?"

#~ msgid ""
#~ "This package only supports MySQL and PostGreSQL database management "
#~ "systems."
#~ msgstr ""
#~ "Este pacote apenas suporta os sistemas de gestão de bases de dados MySQL "
#~ "e PostGreSQL."

#~ msgid "Ident-based"
#~ msgstr "Baseado em Ident"

#~ msgid "Password"
#~ msgstr "Palavra-passe"

#~ msgid "Which authentication method?"
#~ msgstr "Qual método de autenticação ?"

#~ msgid ""
#~ "Please specify which authentication method PostgreSQL uses for the "
#~ "database superuser. The default configuration for PostgreSQL is ident-"
#~ "based authentication."
#~ msgstr ""
#~ "Por favor especifique qual o método de autenticação que o PostgreSQL "
#~ "utiliza para o super-utilizador da base de dados. A configuração por "
#~ "omissão para o PostgreSQL é autenticação baseada em ident."

#~ msgid "What is the name of your Sympa database?"
#~ msgstr "Qual é o nome da sua base de dados Sympa?"

#~ msgid "What is the hostname where your database is running?"
#~ msgstr "Qual é o nome da máquina onde a sua base de dados está a correr?"

#~ msgid "Sympa is able to connect to a local or a remote database."
#~ msgstr "O Sympa é capaz de ligar a uma base de dados local ou remota."

#~ msgid ""
#~ "If you run the database on a local machine, please leave 'localhost' as "
#~ "the hostname."
#~ msgstr ""
#~ "Se correr a base de dados numa máquina local, por favor deixe 'localhost' "
#~ "como nome da máquina."

#~ msgid ""
#~ "Make sure that you properly installed and configured a database server on "
#~ "the host running the database. Database server packages are respectively "
#~ "named `mysql-server' and `postgresql' for MySQL and PostgreSQL."
#~ msgstr ""
#~ "Assegure-se que instalou e configurou correctamente um servidor de bases "
#~ "de dados na máquina que aloja a base de dados. Os pacotes de servidores "
#~ "de bases de dados são chamados `mysql-server' e `postgresql' para MySQL e "
#~ "PostgreSQL, respectivamente."

#~ msgid ""
#~ "Accesses to distant databases are achieved through TCP connections. If "
#~ "you run the database on a distant machine, make sure you configured your "
#~ "database server for TCP connections and you setup the right port."
#~ msgstr ""
#~ "Acesso a bases de dados distantes são alcançados através de ligações TCP. "
#~ "Se você correr a base de dados numa máquina distante, assegure-se que "
#~ "configurou o seu servidor de bases de dados numa máquina distante, "
#~ "assegure-se que configurou o seu servidor de bases de dados para aceitar "
#~ "ligações TCP e configurou o porto correcto."

#~ msgid "What is the TCP port of your ${database} database server?"
#~ msgstr "Qual é o porto TCP do seu servidor de bases de dados ${database}?"

#~ msgid ""
#~ "This TCP port is used by the database server for distant database "
#~ "connections."
#~ msgstr ""
#~ "Este é o porto TCP que é utilizado pelo servidor de bases de dados para "
#~ "ligações a bases de dados remotas."

#~ msgid ""
#~ "You need to set up this parameter when the hostname is different from "
#~ "`localhost'."
#~ msgstr ""
#~ "Você necessita de configurar este parâmetro quando o nome da máquina for "
#~ "diferente de `localhost'."

#~ msgid "What is the user for the database ${dbname}?"
#~ msgstr "Qual é o utilizador para a base de dados ${dbname}?"

#~ msgid "Please specify the user to connect to the database ${dbname}."
#~ msgstr ""
#~ "Por favor especifique o utilizador para ligar á base de dados ${dbname}."

#~ msgid "Password for the database user ${user}:"
#~ msgstr "Palavra-passe para o utilizador ${user} da base de dados:"

#~ msgid ""
#~ "Please supply the password to connect to the database ${dbname} as user "
#~ "${user}."
#~ msgstr ""
#~ "Por favor forneça a palavra-passe para ligar á base de dados ${dbname} "
#~ "como utilizador ${user}."

#~ msgid "Re-enter user sympa password for verification:"
#~ msgstr ""
#~ "Introduza novamente a palavra-passe do utilizador sympa para verificação:"

#~ msgid "Please enter special options for your database connections."
#~ msgstr ""
#~ "Por favor introduza as opções especiais para as suas ligações à base de "
#~ "dados."

#~ msgid ""
#~ "Special options are extra configuration parameters that could be required "
#~ "by database connections in some cases."
#~ msgstr ""
#~ "As opções especiais são parâmetros de configuração extra que podem ser "
#~ "necessários, em alguns casos, pelas ligações à base de dados."

#~ msgid "  mysql_read_default_file=/home/joe/my.cnf"
#~ msgstr "  mysql_read_default_file=/home/joe/my.cnf"

#~ msgid ""
#~ "You can leave the field blank if the database connections don't need any "
#~ "special options."
#~ msgstr ""
#~ "Você pode deixar este campo vazio se as ligações à base de dados não "
#~ "necessitam de quaisquer opções especiais."

#~ msgid "Have you already configured your Sympa database?"
#~ msgstr "Já configurou a sua base de dados Sympa?"

#~ msgid ""
#~ "If you are upgrading, or have already configured your database for use "
#~ "with Sympa for any other reason, you'll want to agree here."
#~ msgstr ""
#~ "Se está a fazer uma actualização, ou se por qualquer outra razão já "
#~ "configurou a sua base de dados para ser utilizada com o Sympa, vai querer "
#~ "concordar aqui."

#~ msgid ""
#~ "If you want your database to be automatically created by this setup "
#~ "program or if you want to reconfigure you database, please disagree."
#~ msgstr ""
#~ "Se quiser que a sua base de dados seja automaticamente criada por este "
#~ "programa de configuração ou se quer reconfigurar você a base de dados, "
#~ "por favor discorde."

#~ msgid ""
#~ "If this is the first time you configure Sympa for using a database, you "
#~ "may want to import you old list subscriber files into the database. For "
#~ "that purpose, this package includes a script called `load_subscribers."
#~ "pl', which can be found in /usr/share/doc/sympa/examples/script."
#~ msgstr ""
#~ "Se esta é a primeira vez que configura o Sympa para utilizar uma base de "
#~ "dados, pode querer importar os seus ficheiros antigos de assinantes da "
#~ "lista para a sua base de dados. Para esse propósito, este pacote inclui "
#~ "um script chamado `load_subscribers.pl', que pode ser encontrado em /usr/"
#~ "share/doc/sympa/examples/script."

#~ msgid "What is your ${database} database admin password?"
#~ msgstr ""
#~ "Qual é a sua password de administrador da base de dados ${database}?"

#~ msgid ""
#~ "Enter the password for your administrator to access the database account. "
#~ "This is most likely not the same password that sympa will use."
#~ msgstr ""
#~ "Introduza a password para o seu administrador aceder à conta da base de "
#~ "dados. O mais provável que não seja a mesma password que o sympa irá "
#~ "utilizar."

#~ msgid ""
#~ "The administrator user for PostgreSQL and MySQL is respectively "
#~ "`postgres' and `root'."
#~ msgstr ""
#~ "O utilizador administrador para o PostgreSQL e para o MySQL são "
#~ "respectivamente `postgres' e `root'."

#~ msgid "Should the subscriber database be removed?"
#~ msgstr "Deve a base de dados de assinantes ser removida?"

#~ msgid ""
#~ "Please choose whether you want to remove the Sympa subscriber database."
#~ msgstr ""
#~ "Por favor escolha se quer remover a base de dados de assinantes do Sympa."

#~ msgid ""
#~ "The lists home directory may also contains mailing list archives if you "
#~ "decided to keep them when you uninstalled the `wwsympa' package."
#~ msgstr ""
#~ "O directório home das listas também pode conter arquivos de mailing lists "
#~ "se você decidiu mantê-los quando desinstalou o pacote `wwsympa'."

#~ msgid ""
#~ "This password is used by the user sympa to connect to the subscribers "
#~ "database."
#~ msgstr ""
#~ "Esta password é utilizada pelo utilizador sympa para ligar à base de "
#~ "dados de assinantes."

#~ msgid ""
#~ "You can leave this field blank if you don't want authenticated accesses "
#~ "to databases."
#~ msgstr ""
#~ "você pode deixar este campo vazio se não quiser acessos autenticados às "
#~ "bases de dados."

#~ msgid "Apache"
#~ msgstr "Apache"

#~ msgid "Apache-SSL"
#~ msgstr "Apache-SSL"

#~ msgid ""
#~ "Once installed, you will be able to access the SOAP server at the "
#~ "following address."
#~ msgstr ""
#~ "Uma vez instalado, você poderá aceder ao servidor SOAP a partir do "
#~ "seguinte endereço."
