# glare debconf portuguese messages
# Copyright (C) 2013 THE glare COPYRIGHT HOLDER
# This file is distributed under the same license as the glare package.
# Pedro Ribeiro <p.m42.ribeiro@gmail.com>, 2013-2014, 2017
#
msgid ""
msgstr ""
"Project-Id-Version: glare 0.1.0-3\n"
"Report-Msgid-Bugs-To: glare@packages.debian.org\n"
"POT-Creation-Date: 2016-09-30 16:26+0200\n"
"PO-Revision-Date: 2017-09-11 10:07+0100\n"
"Last-Translator: Pedro Ribeiro <p.m42.ribeiro@gmail.com>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../glare-common.templates:2001
msgid "Set up a database for Glare?"
msgstr "Configurar uma base de dados para o Glare?"

#. Type: boolean
#. Description
#: ../glare-common.templates:2001
msgid ""
"No database has been set up for Glare to use. Before continuing, you should "
"make sure you have the following information:"
msgstr ""
"N�o foi definida nenhuma base de dados para uso do Glare. Antes de "
"continuar, deve certificar-se que tem a seguinte informa��o:"

#. Type: boolean
#. Description
#: ../glare-common.templates:2001
msgid ""
" * the type of database that you want to use;\n"
" * the database server hostname (that server must allow TCP connections from "
"this\n"
"   machine);\n"
" * a username and password to access the database."
msgstr ""
" * o tipo de base de dados que deseja usar;\n"
" * o nome da m�quina do servidor da base de dados (que tem que permitir\n"
"    liga��es TCP a partir desta m�quina);\n"
" * um nome de utilizador e palavra-passe para aceder � base de dados."

#. Type: boolean
#. Description
#: ../glare-common.templates:2001
msgid ""
"If some of these requirements are missing, do not choose this option and run "
"with regular SQLite support."
msgstr ""
"Se n�o tem alguns destes requisitos, n�o escolha esta op��o e corra com "
"suporte SQLite normal."

#. Type: boolean
#. Description
#: ../glare-common.templates:2001
msgid ""
"You can change this setting later on by running \"dpkg-reconfigure -plow "
"glare\"."
msgstr ""
"Pode mudar esta defini��o mais tarde executando \"dpkg-reconfigure -plow "
"glare\"."

#. Type: string
#. Description
#: ../glare-common.templates:3001
msgid "Authentication server hostname:"
msgstr "Nome do servidor de autentica��o:"

#. Type: string
#. Description
#: ../glare-common.templates:3001
msgid ""
"Please specify the hostname of the authentication server. Typically this is "
"also the hostname of the OpenStack Identity Service (Keystone)."
msgstr ""
"Por favor especifique o nome de m�quina do servidor de autentica��o. "
"Tipicamente este � tamb�m o nome de m�quina do Servi�o de Identidade "
"OpenStack (Keystone)."

#. Type: string
#. Description
#. Translators: a "tenant" in OpenStack world is
#. an entity that contains one or more username/password couples.
#. It's typically the tenant that will be used for billing. Having more than one
#. username/password is very helpful in larger organization.
#. You're advised to either keep "tenant" without translating it
#. or keep it aside with your translation. Example for French:
#. proprietaire ("tenant")
#: ../glare-common.templates:4001
msgid "Authentication server tenant name:"
msgstr "Nome do inquilino ('tenant') do servidor de autentica��o:"

#. Type: string
#. Description
#. Translators: a "tenant" in OpenStack world is
#. an entity that contains one or more username/password couples.
#. It's typically the tenant that will be used for billing. Having more than one
#. username/password is very helpful in larger organization.
#. You're advised to either keep "tenant" without translating it
#. or keep it aside with your translation. Example for French:
#. proprietaire ("tenant")
#: ../glare-common.templates:4001
msgid "Please specify the authentication server tenant name."
msgstr "Indique por favor o nome 'tenant' do servidor de autentica��o."

#. Type: string
#. Description
#: ../glare-common.templates:5001
msgid "Authentication server username:"
msgstr "Nome de utilizador do servidor de autentica��o:"

#. Type: string
#. Description
#: ../glare-common.templates:5001
msgid "Please specify the username to use with the authentication server."
msgstr ""
"Indique por favor o nome de utilizador a usar com o servidor de autentica��o."

#. Type: password
#. Description
#: ../glare-common.templates:6001
msgid "Authentication server password:"
msgstr "Palavra-passe do servidor de autentica��o:"

#. Type: password
#. Description
#: ../glare-common.templates:6001
msgid "Please specify the password to use with the authentication server."
msgstr ""
"Indique por favor a palavra-passe a usar com o servidor de autentica��o."

#. Type: string
#. Description
#: ../glare-common.templates:7001
msgid "IP address of your RabbitMQ host:"
msgstr "Endere�o IP da sua m�quina RabbitMQ:"

#. Type: string
#. Description
#. Type: string
#. Description
#. Type: password
#. Description
#: ../glare-common.templates:7001 ../glare-common.templates:8001
#: ../glare-common.templates:9001
msgid ""
"In order to interoperate with other components of OpenStack, this package "
"needs to connect to a central RabbitMQ server."
msgstr ""
"De modo a conseguir trabalhar com outros componentes do OpenStack, este "
"pacote precisa de se ligar a um servidor central RabbitMQ."

#. Type: string
#. Description
#: ../glare-common.templates:7001
msgid "Please specify the IP address of that server."
msgstr "Indique por favor o endere�o IP desse servidor."

#. Type: string
#. Description
#: ../glare-common.templates:8001
msgid "Username for connection to the RabbitMQ server:"
msgstr "Nome de utilizador para liga��o ao servidor RabbitMQ:"

#. Type: string
#. Description
#: ../glare-common.templates:8001
msgid "Please specify the username used to connect to the RabbitMQ server."
msgstr ""
"Indique por favor o nome de utilizador para se ligar ao servidor RabbitMQ."

#. Type: password
#. Description
#: ../glare-common.templates:9001
msgid "Password for connection to the RabbitMQ server:"
msgstr "Palavra-passe para liga��o ao servidor RabbitMQ:"

#. Type: password
#. Description
#: ../glare-common.templates:9001
msgid "Please specify the password used to connect to the RabbitMQ server."
msgstr ""
"Indique por favor a palavra-passe usada para ligar ao servidor RabbitMQ."

#. Type: boolean
#. Description
#: ../glare-api.templates:2001
msgid "Register Glare in the Keystone endpoint catalog?"
msgstr "Registar o Glare no cat�logo do ponto de liga��o do Keystone?"

#. Type: boolean
#. Description
#: ../glare-api.templates:2001
msgid ""
"Each OpenStack service (each API) should be registered in order to be "
"accessible. This is done using \"keystone service-create\" and \"keystone "
"endpoint-create\". This can be done automatically now."
msgstr ""
"Cada servi�o OpenStack (cada API) deve ser registado de modo a ser "
"acess�vel. Isto � feito atrav�s do \"keystone service-create\" e \"keystone "
"endpoint-create\". Isto pode ser feito automaticamente agora."

#. Type: boolean
#. Description
#: ../glare-api.templates:2001
msgid ""
"Note that you will need to have an up and running Keystone server on which "
"to connect using the Keystone authentication token."
msgstr ""
"Note que ir� precisar de ter um servidor Keystone a correr onde se ligar "
"para se autenticar com um sinal de autentica��o do Keystone."

#. Type: string
#. Description
#: ../glare-api.templates:3001
msgid "Keystone server IP address:"
msgstr "Endere�o IP do Keystone:"

#. Type: string
#. Description
#: ../glare-api.templates:3001
msgid ""
"Please enter the IP address of the Keystone server, so that glare-api can "
"contact Keystone to do the Glare service and endpoint creation."
msgstr ""
"Indique o endere�o IP do seu servidor Keystone, de modo a que o glare-api "
"possa contactar o Keystone para criar o servi�o Glare e o ponto de liga��o."

#. Type: string
#. Description
#: ../glare-api.templates:4001
msgid "Keystone admin name:"
msgstr "Nome do administrador do Keystone:"

#. Type: string
#. Description
#. Type: string
#. Description
#. Type: password
#. Description
#: ../glare-api.templates:4001 ../glare-api.templates:5001
#: ../glare-api.templates:6001
msgid ""
"To register the service endpoint, this package needs to know the Admin "
"login, name, project name, and password to the Keystone server."
msgstr ""
"Para registar o endpoint do servi�o, o pacote necessita de saber o nome de "
"utilizador, o nome, o nome de projecto e a password do Administrador do "
"servidor Keystone."

#. Type: string
#. Description
#: ../glare-api.templates:5001
msgid "Keystone admin project name:"
msgstr "Nome do projecto do administrador Keystone:"

#. Type: password
#. Description
#: ../glare-api.templates:6001
msgid "Keystone admin password:"
msgstr "Password de administrador Keystone:"

#. Type: string
#. Description
#: ../glare-api.templates:7001
msgid "Glare endpoint IP address:"
msgstr "Endere�o IP do ponto de liga��o Glare:"

#. Type: string
#. Description
#: ../glare-api.templates:7001
msgid "Please enter the IP address that will be used to contact Glare."
msgstr "Indique o endere�o IP que ser� usado para contactar com o Glare."

#. Type: string
#. Description
#: ../glare-api.templates:7001
msgid ""
"This IP address should be accessible from the clients that will use this "
"service, so if you are installing a public cloud, this should be a public IP "
"address."
msgstr ""
"Este endere�o IP dever� ser acess�vel a partir dos clientes que usar�o este "
"servi�o, portanto se est� a instalar uma cloud p�blica, este deve ser um "
"endere�o IP p�blico."

#. Type: string
#. Description
#: ../glare-api.templates:8001
msgid "Name of the region to register:"
msgstr "Nome da regi�o a registar:"

#. Type: string
#. Description
#: ../glare-api.templates:8001
msgid ""
"OpenStack supports using availability zones, with each region representing a "
"location. Please enter the zone that you wish to use when registering the "
"endpoint."
msgstr ""
"O OpenStack suporta a utiliza��o de zonas de disponibilidade, com cada "
"regi�o a representar uma localiza��o. Por favor, indique a zona que quer "
"usar ao registar o ponto de liga��o."

#~ msgid "Glare volume group:"
#~ msgstr "Grupo de volume do Glare:"

#~ msgid ""
#~ "Please specify the name of the LVM volume group on which Glare will "
#~ "create partitions."
#~ msgstr ""
#~ "Indique por favor o nome do grupo de volume LVM no qual o Glare ir� criar "
#~ "parti��es."
