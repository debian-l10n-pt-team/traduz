# Portuguese translation of calamaris.
# This file is distributed under the same license as the calamaris package.
# Manuel Padilha <manuel.padilha@gmail.com>, 2008.
# Pedro Ribeiro <p.m42.ribeiro@gmail.com>, 2011-2012
# 
msgid ""
msgstr ""
"Project-Id-Version: calamaris 2.99.4.0-16\n"
"Report-Msgid-Bugs-To: calamaris@packages.debian.org\n"
"POT-Creation-Date: 2011-06-26 00:29-0500\n"
"PO-Revision-Date: 2012-01-07 10:17+0000\n"
"Last-Translator: Pedro Ribeiro <p.m42.ribeiro@gmail.com>, 2011\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#: ../templates:2001
msgid "auto"
msgstr "auto"

#. Type: select
#. Choices
#: ../templates:2001
msgid "squid"
msgstr "squid"

#. Type: select
#. Choices
#: ../templates:2001
msgid "squid3"
msgstr "squid3"

#. Type: select
#. Description
#: ../templates:2002
msgid "Type of proxy log files to analyze:"
msgstr "Tipo de ficheiros de 'log' de proxy a analisar:"

#. Type: select
#. Description
#: ../templates:2002
msgid ""
"Calamaris is able to process log files from Squid or Squid3. If you choose "
"'auto' it will look first for Squid log files and then for Squid3 log files."
msgstr ""
"O Calamaris � capaz de processar ficheiros de 'log' do Squid ou do Squid3. "
"Se escolher 'auto' ser�o procurados os ficheiros de 'log' do Squid em "
"primeiro lugar e s� depois os do Squid3."

#. Type: select
#. Description
#: ../templates:2002
msgid ""
"Choosing 'auto' is recommended when only one proxy is installed. Otherwise, "
"the appropriate setting can be enforced here."
msgstr ""
"Ecolher 'auto' � a op��o recomendada quando tem apenas um proxy instalado. "
"Caso contr�rio, poder� ser especificada aqui a op��o correta."

#. Type: select
#. Choices
#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../templates:3001 ../templates:7001 ../templates:11001
msgid "nothing"
msgstr "nenhum"

#. Type: select
#. Choices
#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../templates:3001 ../templates:7001 ../templates:11001
msgid "mail"
msgstr "mail"

#. Type: select
#. Choices
#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../templates:3001 ../templates:7001 ../templates:11001
msgid "web"
msgstr "web"

#. Type: select
#. Choices
#. Type: select
#. Choices
#. Type: select
#. Choices
#: ../templates:3001 ../templates:7001 ../templates:11001
msgid "both"
msgstr "ambos"

#. Type: select
#. Description
#: ../templates:3002
msgid "Output method for Calamaris daily analysis reports:"
msgstr "M�todo de envio dos relat�rios di�rios do Calamaris:"

#. Type: select
#. Description
#. Type: select
#. Description
#. Type: select
#. Description
#: ../templates:3002 ../templates:7002 ../templates:11002
msgid ""
"The result of the Calamaris analysis can be sent as an email to a specified "
"address or stored as a web page."
msgstr ""
"O resultado da an�lise do Calamaris pode ser enviado por email para um "
"determinado endere�o, ou guardado como uma p�gina web."

#. Type: select
#. Description
#. Type: select
#. Description
#. Type: select
#. Description
#: ../templates:3002 ../templates:7002 ../templates:11002
msgid "Please choose which of these methods you want to use."
msgstr "Indique por favor qual dos m�todos pretende usar."

#. Type: string
#. Description
#: ../templates:4001
msgid "Recipient for daily analysis reports by mail:"
msgstr "Destinat�rio dos emails de an�lise di�ria:"

#. Type: string
#. Description
#: ../templates:4001
msgid ""
"Please choose the address that should receive daily Calamaris analysis "
"reports."
msgstr ""
"Indique por favor o endere�o de email que deve receber diariamente os "
"relat�rios do Calamaris."

#. Type: string
#. Description
#. Type: string
#. Description
#. Type: string
#. Description
#: ../templates:4001 ../templates:8001 ../templates:12001
msgid "This setting is only needed if the reports are to be sent by email."
msgstr ""
"Esta defini��o s� � necess�ria se os relat�rios forem enviados por email."

#. Type: string
#. Description
#: ../templates:5001
msgid "Directory for storing HTML daily analysis reports:"
msgstr "Direct�rio onde armazenar os relat�rios di�rios em HTML:"

#. Type: string
#. Description
#: ../templates:5001
msgid ""
"Please choose the directory where daily Calamaris analysis reports should be "
"stored."
msgstr ""
"Indique por favor o direct�rio onde os relat�rios di�rios do Calamaris devem "
"ser guardados."

#. Type: string
#. Description
#. Type: string
#. Description
#. Type: string
#. Description
#: ../templates:5001 ../templates:9001 ../templates:13001
msgid "This setting is only needed if the reports are to be generated as HTML."
msgstr "Esta defini��o s� � necess�ria se os relat�rios forem gerados em HTML."

#. Type: string
#. Description
#: ../templates:6001
msgid "Title of the daily analysis reports:"
msgstr "T�tulo da an�lise di�ria:"

#. Type: string
#. Description
#: ../templates:6001
msgid ""
"Please choose the text that will be used as a prefix to the title for the "
"daily Calamaris analysis reports."
msgstr ""
"Indique por favor o texto a ser usado como prefixo do t�tulo do relat�rio "
"di�rio do Calamaris."

#. Type: select
#. Description
#: ../templates:7002
msgid "Output method for Calamaris weekly analysis reports:"
msgstr "M�todo de envio dos relat�rios semanais do Calamaris:"

#. Type: string
#. Description
#: ../templates:8001
msgid "Recipient for weekly analysis reports by mail:"
msgstr "Destinat�rio dos emails de an�lise semanal:"

#. Type: string
#. Description
#: ../templates:8001
msgid ""
"Please choose the address that should receive weekly Calamaris analysis "
"reports."
msgstr ""
"Indique por favor o endere�o de email que deve receber semanalmente os "
"relat�rios do Calamaris."

#. Type: string
#. Description
#: ../templates:9001
msgid "Directory for storing HTML weekly analysis reports:"
msgstr "Direct�rio onde armazenar os relat�rios semanais em HTML:"

#. Type: string
#. Description
#: ../templates:9001
msgid ""
"Please choose the directory where weekly Calamaris analysis reports should "
"be stored."
msgstr ""
"Indique por favor o direct�rio onde os relat�rios semanais do Calamaris "
"devem ser guardados."

#. Type: string
#. Description
#: ../templates:10001
msgid "Title of the weekly analysis reports:"
msgstr "T�tulo da an�lise semanal:"

#. Type: string
#. Description
#: ../templates:10001
msgid ""
"Please choose the text that will be used as a prefix to the title for the "
"weekly Calamaris analysis reports."
msgstr ""
"Indique por favor o texto a ser usado como prefixo do t�tulo do relat�rio "
"semanal do Calamaris."

#. Type: select
#. Description
#: ../templates:11002
msgid "Output method for Calamaris monthly analysis reports:"
msgstr "M�todo de envio dos relat�rios mensais do Calamaris:"

#. Type: string
#. Description
#: ../templates:12001
msgid "Recipient for monthly analysis reports by mail:"
msgstr "Destinat�rio dos emails de an�lise mensal:"

#. Type: string
#. Description
#: ../templates:12001
msgid ""
"Please choose the address that should receive monthly Calamaris analysis "
"reports."
msgstr ""
"Indique por favor o endere�o de email que deve receber mensalmente os "
"relat�rios do Calamaris."

#. Type: string
#. Description
#: ../templates:13001
msgid "Directory for storing HTML monthly analysis reports:"
msgstr "Direct�rio onde armazenar os relat�rios mensais em HTML:"

#. Type: string
#. Description
#: ../templates:13001
msgid ""
"Please choose the directory where monthly Calamaris analysis reports should "
"be stored."
msgstr ""
"Indique por favor o direct�rio onde os relat�rios mensais do Calamaris devem "
"ser guardados."

#. Type: string
#. Description
#: ../templates:14001
msgid "Title of the monthly analysis reports:"
msgstr "T�tulo dos relat�rios da an�lise mensal:"

#. Type: string
#. Description
#: ../templates:14001
msgid ""
"Please choose the text that will be used as a prefix to the title for the "
"monthly Calamaris analysis reports."
msgstr ""
"Indique por favor o texto a ser usado como prefixo do t�tulo do relat�rio "
"mensal do Calamaris."

#. Type: string
#. Description
#: ../templates:15001
msgid "Names of proxy log files to analyze:"
msgstr "Nomes dos ficheiros de 'log' de proxy a analisar:"

#. Type: string
#. Description
#: ../templates:15001
msgid ""
"Calamaris is supposed to run before the logrotation of the Squid logfiles. "
"If you want to let it run right after the logrotation of Squid (e.g. by "
"adding Calamaris to /etc/logrotate.d/squid), you should change this entry to "
"access.log.1."
msgstr ""
"� suposto correr o Calamaris antes da rota��o de logs do Squid. Se o quiser "
"correr logo ap�s a rota��o (e.g. ao adicionar o Calamaris a "
"/etc/logrotate.d/squid), deve alterar esta defini��o para access.log.1."

