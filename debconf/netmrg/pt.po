# Portuguese translation of netmrg debconf messages.
# Copyright (C) 2006 Free Software Foundation, Inc.
# Rui Branco <ruipb@debianpt.org>, 2006.
# 05-06-2007 - Rui Branco <ruipb@debianpt.org>
#
msgid ""
msgstr ""
"Project-Id-Version: netmrg 0.18.2-13\n"
"Report-Msgid-Bugs-To: steinm@debian.org\n"
"POT-Creation-Date: 2007-05-30 08:32+0200\n"
"PO-Revision-Date: 2007-06-05 18:31+0100\n"
"Last-Translator: Rui Branco <ruipb@debianpt.org>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit"

#. Type: select
#. choices
#: ../templates:1001
msgid "None, Apache2, Apache, Apache-SSL, All"
msgstr "Nenhum, Apache2, Apache, Apache-SSL, Todos"

#. Type: select
#. description
#: ../templates:1002
msgid "Web server to configure:"
msgstr "Servidor web a configurar:"

#. Type: string
#. default
#: ../templates:2001
msgid "localhost"
msgstr "máquina local"

#. Type: string
#. description
#: ../templates:2002
msgid "Host name of the mysql database server:"
msgstr "Nome de máquina do servidor de base de dados mysql:"

#. Type: string
#. default
#. Type: string
#. default
#: ../templates:3001 ../templates:5001
msgid "netmrg"
msgstr "netmrg"

#. Type: string
#. description
#: ../templates:3002
msgid "Name for the database:"
msgstr "Nome para a base de dados:"

#. Type: note
#. description
#: ../templates:4002
msgid "The database already exists"
msgstr "A base de dados já existe"

#. Type: note
#. description
#: ../templates:4002
msgid ""
"A simple test revealed that a database with the name \"${dbname}\" already "
"exists and appears to be a netmrg database. If you really like to use this "
"database then confirm the name in the next step."
msgstr ""
"Um teste simples revelou que uma base de dados com o nome \"${dbname}\" já "
"existe e parece ser uma base de dados netmrg. Se realmente deseja usar esta "
"base de dados confirme o nome no próximo passo."

#. Type: string
#. description
#: ../templates:5002
msgid "Name of the database user:"
msgstr "Nome do utilizador da base de dados:"

#. Type: string
#. description
#: ../templates:5002
msgid "This user will have read/write access to the netmrg database."
msgstr "Este utilizador terá acesso de leitura/escrita à base de dados netmrg."

#. Type: password
#. description
#: ../templates:6002
msgid "Password of the database user:"
msgstr "Palavra-chave do utilizador da base de dados:"

#. Type: password
#. description
#: ../templates:7002
msgid "Confirm the password of the database user:"
msgstr "Confirme a palavra-chave do utilizador da base de dados:"

#. Type: text
#. description
#: ../templates:8002
msgid "Password mismatch"
msgstr "A palavra-chave não coincide"

#. Type: text
#. description
#: ../templates:8002
msgid "The database user passwords you entered didn't match. Please try again."
msgstr "As palavras-chaves que introduziu não coincidem. Por favor tente outra vez."

#. Type: string
#. description
#: ../templates:9002
msgid "Name of the database system administrator:"
msgstr "Nome do administrador da base de dados sistema:"

#. Type: string
#. description
#: ../templates:9002
msgid ""
"In order to be able to create the initial database and database user for "
"netmrg, the account name of the database system administrator is needed."
msgstr ""
"Para ser capaz de criar a base de dados inicial e um utilizador da mesma "
"para o netmrg, necessita do nome da conta do administrador da base de dados "
"sistema."

#. Type: password
#. description
#: ../templates:10002
msgid "Password of the database system administrator:"
msgstr "Palavra-chave do administrador da base de dados sistema:"

#. Type: string
#. description
#: ../templates:11002
msgid "Name of web host:"
msgstr "Nome da máquina do servidor web:"

#. Type: string
#. description
#: ../templates:11002
msgid ""
"netmrg's user interface is web based. The hostname of the web server is used "
"for urls within the netmrg web interface. Leaving this entry field empty is "
"usually a good choice."
msgstr ""
"O interface para o utilizador do netmrg é baseado na web. O nome da máquina "
"servidor web é utilizado para 'urls' dentro da interface web do netmrg. "
"Deixar esta entrada em branco normalmente é uma boa opção."

#. Type: string
#. description
#: ../templates:12002
msgid "Name of your company:"
msgstr "Nome da sua companhia:"

#. Type: string
#. description
#: ../templates:12002
msgid ""
"The web front end of netmrg shows the name of your company in the header of "
"each web page."
msgstr ""
"O interface de visualização web do netmrg mostra o nome da sua companhia no "
"cabeçalho cada página."

#. Type: string
#. default
#: ../templates:13001
msgid "http://"
msgstr "http://"

#. Type: string
#. description
#: ../templates:13002
msgid "Url of your companies website:"
msgstr "'Url' da página web da sua empresa:"

#. Type: string
#. description
#: ../templates:13002
msgid ""
"Clicking on the company name will follow a link. It is usualy the url "
"pointing to the start page of your company's web site."
msgstr ""
"Ao clicar no nome da companhia será activado o link. Normalmente é o link "
"'url' que aponta para a página de início do site da sua companhia."

#. Type: boolean
#. default
#: ../templates:14001
msgid "true"
msgstr "verdadeiro"

#. Type: boolean
#. description
#: ../templates:14002
msgid "Delete data after purging software?"
msgstr "Apagar data depois de eliminar o software?"

#. Type: boolean
#. description
#: ../templates:14002
msgid ""
"Purging a package usually removes all data collected by the software "
"including the log files. Choose 'no' if you would like to keep the mysql "
"database and the datafiles even if the netmrg package is purged sometime in "
"the future."
msgstr ""
"Eliminar um pacote normalmente elimina toda a informação recolhida pelo "
"pacote incluindo ficheiros de relatório. Escolha 'não' se preferir manter a "
"base de dados mysql e os ficheiros de data, mesmo que o pacote netmrg seja "
"eliminado um dia mais tarde."

#. Type: note
#. description
#: ../templates:15002
msgid "Package configuration note"
msgstr "Nota de configuração do pacote"

#. Type: note
#. description
#: ../templates:15002
msgid ""
"The package has been successfully installed and configured. Point your "
"webbrowser towards http://${site}/netmrg/ and log in as user 'admin' with "
"the password 'nimda'. Make sure to change the password as soon as possible "
"since it is the default password for all netmrg installations."
msgstr ""
"O pacote foi instalado e configurado com sucesso. Aponte o seu cliente web "
"para http://${site}/netmrg/ e entre como um utilizador 'admin' com a palavra-"
"chave 'nimda'. Garanta que a palavra-chave seja modificada o mais depressa "
"possível, já que é a palavra-chave por omissão para todas as instalações "
"netmrg."

#. Type: note
#. description
#: ../templates:16002
msgid "Error while creating database"
msgstr "Erro ao criar a base de dados"

#. Type: note
#. description
#: ../templates:17002
msgid "Error while creating database user"
msgstr "Erro ao criar o utilizador da base de dados"

#. Type: note
#. description
#: ../templates:18002
msgid "Error while creating database tables"
msgstr "Erro ao criar as tabelas da base de dados"

