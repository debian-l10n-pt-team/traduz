# zekr-quran-translations-en debconf messages translation
# Copyright (C) 2008
# This file is distributed under the same license as the zekr-quran-translations-en package.
# Carlos Lisboa <carloslisboa@gmail.com>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: zekr-quran-translations-en\n"
"Report-Msgid-Bugs-To: zekr.debian@gmail.com\n"
"POT-Creation-Date: 2007-10-30 06:40+0100\n"
"PO-Revision-Date: 2008-04-10 22:37+0100\n"
"Last-Translator: Carlos Lisboa <carloslisboa@gmail.com>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: note
#. Description
#: ../templates:2001
msgid "No authenticity warranty"
msgstr "Sem garantia de autenticicidade"

#. Type: note
#. Description
#: ../templates:2001
msgid ""
"This package provides a collection of translations which are publicly "
"available on the web."
msgstr ""
"Este pacote disponibiliza uma colecção de traduções disponíveis publicamente "
"na internet."

#. Type: note
#. Description
#: ../templates:2001
msgid ""
"According to Islamic theology, perfectly translating the Quran is "
"impossible. Most of the time, English translations are considered to be "
"'interpretations', side by side with the Arabic text, attempting to provide "
"the best expression of its meaning."
msgstr ""
"De acordo com a teologia Islâmica, traduzir o Quran na perfeição é "
"impossível. A maioria das vezes, as traduções Inglesas são consideradas "
"'interpretações', lado a lado com o texto Arábico, tentando dispononibilizar "
"a melhor expressão para o seu significado."

#. Type: note
#. Description
#: ../templates:2001
msgid ""
"For more information, please read the /usr/share/doc/zekr-quran-translations-"
"en/README.txt file."
msgstr ""
"Para mais informações, leia o ficheiro /usr/share/doc/zekr-quran-translations-"
"en/README.txt."

#. Type: note
#. Description
#: ../templates:3001
msgid "Commercial redistribution prohibited"
msgstr "É proibida a redistribuição comercial"

#. Type: note
#. Description
#: ../templates:3001
msgid ""
"The contents of this package should not be redistributed for commercial "
"purposes. It is only provided to help readers to learn more about Islam."
msgstr ""
"Os conteúdos deste pacote não devem ser redistribuidos com intuito "
"comercial. É apenas disponibilizado para ajudar leitores a aprender mais "
"sobre o Islão."

#. Type: boolean
#. Description
#: ../templates:4001
msgid "Do you agree with the license terms?"
msgstr "Aceita os termos da licença?"

#. Type: boolean
#. Description
#: ../templates:4001
msgid ""
"In order to install this package, please agree to the licensing terms, "
"particularly with the prohibition of commercial distribution."
msgstr ""
"Para instalar este pacote, por favor aceite os termos da licença, "
"em particular com a proibição da distribuição comercial."

#. Type: error
#. Description
#: ../templates:5001
msgid "Declined Quran translations license terms"
msgstr "Recusa dos termos da licença da tradução Quran"

#. Type: error
#. Description
#: ../templates:5001
msgid ""
"As you declined this package's license terms, its installation will be "
"aborted."
msgstr ""
"Como recusou os termos da licença deste pacote, a sua instalação será "
"abortada."
