# translation of rtpg_0.0.3-2 debconf to Portuguese
# Copyright (C) 2009 THE RTPG'S COPYRIGHT HOLDER
# This file is distributed under the same license as the rtpg package.
#
# Américo Monteiro <a_monteiro@netcabo.pt>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: rtpg 0.0.3-2\n"
"Report-Msgid-Bugs-To: rtpg@packages.debian.org\n"
"POT-Creation-Date: 2009-01-12 07:02+0100\n"
"PO-Revision-Date: 2009-01-12 13:13+0000\n"
"Last-Translator: Américo Monteiro <a_monteiro@netcabo.pt>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#. Type: boolean
#. Description
#: ../rtpg-www.templates:2001
msgid "Add an entry for the virtual server in /etc/hosts?"
msgstr "Adicionar uma entrada para o servidor virtual em /etc/hosts?"

#. Type: boolean
#. Description
#: ../rtpg-www.templates:2001
msgid "This package may define a virtual server in the web server configuration."
msgstr "Este pacote pode definir um servidor virtual na configuração do servidor web."

#. Type: boolean
#. Description
#: ../rtpg-www.templates:2001
msgid ""
"For this to be fully functional, an entry is needed in the /etc/hosts file "
"for the virtual server. This operation can be made automatic by enabling "
"this option."
msgstr ""
"Para que isto seja completamente funcional, é necessário uma entrada no "
"ficheiro /etc/hosts para o servidor virtual. Esta operação pode ser feita "
"automaticamente ao activar esta opção."

