# translation of cgiemail debconf to Portuguese
# Portuguese translation for cgiemail's debconf messages
# This file is distributed under the same license as the cgiemail package.
#
# Ricardo Silva <ardoric@gmail.com>, 2006.
# Américo Monteiro <a_monteiro@netcabo.pt>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: cgiemail 1.6-32\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-04-07 04:30+0200\n"
"PO-Revision-Date: 2008-05-25 23:25+0100\n"
"Last-Translator: Américo Monteiro <a_monteiro@netcabo.pt>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#. Type: string
#. Description
#: ../templates:1001
msgid "Directory where you want to put the cgiemail's mail templates:"
msgstr "Directório onde deseja colocar os modelos de email do cgiemail: "

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"In old versions of cgiemail, templates that were used for creating e-mails "
"to be sent could be placed anywhere that would be served up by the web "
"server.  This behaviour is a security vulnerability: an attacker can read "
"files that he shouldn't be able to, such as scripts in cgi-bin, if they "
"contain certain pieces of text."
msgstr ""
"Em versões antigas do cgiemail, os modelos que eram usados para criar e-mails "
"a enviar podiam ser colocados em qualquer sitio que fosse servido pelo "
"servidor web. Este comportamento é uma vulnerabilidade de segurança: "
"um atacante pode ler ficheiros que não devia, como 'scripts' em cgi-bin, "
"se contiverem certas partes em texto."

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"If you enter nothing (that is, erase the default directory, leaving this "
"empty), cgiemail will still work. This may be needed if you are, for "
"instance, hosting web services, and cannot move all of your clients cgiemail "
"templates to one directory.  Remember that this will LEAVE THE SECURITY HOLE "
"OPEN, and is only a choice for backwards compatibility."
msgstr ""
"Se não introduzir nada (isto é, apagar o directório pré-definido, deixando "
"este campo vazio), o cgiemail ainda funcionará. Isto pode ser necessário se "
"você está, por exemplo, a alojar servidores web e não puder mover todos os "
"modelos do cgiemail dos seus clientes para um único directório. Lembre-se que "
"isto vai DEIXAR O BURACO DE SEGURANÇA ABERTO, e é apenas uma escolha para "
"retro-compatibilidade."

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"To close the hole, enter a directory, which MUST be accessible by your web "
"server.  Template files that you want to use should go there. For further "
"instructions, please read the README.Debian and README files in /usr/share/"
"doc/cgiemail/."
msgstr ""
"Para fechar o 'buraco', introduza um directório, que TEM de estar acessível "
"pelo seu servidor web. Os ficheiros modelo que quiser utilizar devem ir para "
"esse directório. Para mais instruções, por favor leia o ficheiro README.Debian "
"e os ficheiros README em /usr/share/doc/cgiemail/."

