# Native Portuguese translation of mtop debconf.
# Ricardo Silva <ardoric@gmail.com>, 2006.
# 
msgid ""
msgstr ""
"Project-Id-Version: mtop 0.6.6-1\n"
"Report-Msgid-Bugs-To: aroldan@debian.org\n"
"POT-Creation-Date: 2006-11-29 15:59+0100\n"
"PO-Revision-Date: 2007-03-01 22:23+000\n"
"Last-Translator: Ricardo Silva <ardoric@gmail.com>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../mtop.templates:1001
msgid "MySQL host name:"
msgstr "Nome de máquina do MySQL"

#. Type: string
#. Description
#: ../mtop.templates:1001
msgid ""
"Please enter the host name of the MySQL server which you want to monitor."
msgstr ""
"Por favor introduza o nome do servidor MySQL que vai conter a base de dados "
"do mantis."

#. Type: string
#. Description
#: ../mtop.templates:2001
msgid "MySQL port number:"
msgstr "Porto do MySQL:"

#. Type: string
#. Description
#: ../mtop.templates:2001
msgid "Please enter the port number MySQL listens on."
msgstr "Por favor introduza o porto em que o MySQL ouve."

#. Type: string
#. Description
#: ../mtop.templates:3001
msgid "Name of your database's administrative user:"
msgstr "Nome do utilizador administrativo da sua base de dados:"

#. Type: string
#. Description
#: ../mtop.templates:3001
msgid ""
"Please enter the username of MySQL administrator (needed for creating the "
"mysqltop user)."
msgstr ""
"Por favor introduza o nome de utilizador do administrador de MySQL "
"(necessário para a criação do utilizador mysqltop)."

#. Type: password
#. Description
#: ../mtop.templates:4001
msgid "Password of your database's administrative user:"
msgstr "Palavra-chave do utilizador administrador da sua base de dados:"

#. Type: password
#. Description
#: ../mtop.templates:4001
msgid "Enter \"none\" if there is no password for MySQL administration."
msgstr "Introduza \"none\" se não há palavra-chave para administração do MySQL."

