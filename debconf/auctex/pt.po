# Portuguese translations of the "debian/auctex/templates" file of the
# auctex Debian package.
#
# Copyright (C) 2006, 07 Davide G. M. Salvetti
#
# This file is distributed under the same license as the auctex Debian
# package.
#
# Ricardo Silva <ardoric@gmail.com>, 2006, 07
# Pedro Ribeiro <p.m42.ribeiro@gmail.com>, 2011-2012
msgid ""
msgstr ""
"Project-Id-Version: auctex 11.86-7\n"
"Report-Msgid-Bugs-To: auctex@packages.debian.org\n"
"POT-Creation-Date: 2011-12-21 21:24+0100\n"
"PO-Revision-Date: 2012-01-07 10:07+0000\n"
"Last-Translator: Pedro Ribeiro <p.m42.ribeiro@gmail.com>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: select
#. Choices
#: ../auctex/templates:1001
msgid "Background"
msgstr "Segundo plano"

#. Type: select
#. Choices
#: ../auctex/templates:1001
msgid "Foreground"
msgstr "Primeiro plano"

#. Type: select
#. Choices
#: ../auctex/templates:1001
msgid "None"
msgstr "Nenhum"

#. Type: select
#. Description
#: ../auctex/templates:1002
msgid "(La)TeX macros parsing mode:"
msgstr "Modo de análise de macros do (La)TeX:"

#. Type: select
#. Description
#: ../auctex/templates:1002
msgid ""
"To improve the performance of AUCTeX, every currently installed TeX macro "
"package and LaTeX style file will be parsed."
msgstr ""
"Para optimizar o desempenho do AUCTeX, todos os pacotes de macros do TeX e "
"todos os ficheiros de estilo do LaTeX instalados serão analisados."

#. Type: select
#. Description
#. Translators: do NOT translate ${LOGFILE}
#: ../auctex/templates:1002
msgid ""
"This may take a lot of time, so it should probably be done in the "
"background.  You may also choose to have it done in the foreground, or to "
"skip that step.  If you choose 'Background', you will find a detailed log of "
"the process in ${LOGFILE}."
msgstr ""
"Este processo poderá demorar algum tempo, pelo que provavelmente deverá ser "
"executado em segundo plano. Também poderá escolher executá-lo em primeiro "
"plano, ou até saltar o processo de análise. Caso decida executar em segundo "
"plano, um registo detalhado do processo será armazenado no ficheiro "
"${LOGFILE}."

#. Type: select
#. Description
#: ../auctex/templates:1002
msgid ""
"The cached data gets automatically updated via dpkg triggers, so that no "
"specific action is required whenever you install new (La)TeX packages or "
"remove old ones."
msgstr ""
"Os dados em cache são automaticamente actualizados através de triggers do "
"dpkg, de modo que não é necessária nenhuma ação específica quando se "
"instalam ou removem pacotes (La)TeX."

#. Type: select
#. Description
#: ../auctex/templates:1002
msgid ""
"This update can be run manually at any moment by running 'update-auctex-"
"elisp'."
msgstr ""
"Esta actualização pode ser corrida a qualquer momento de uma forma manual "
"correndo o programa 'update-auctex-elisp'."

#. Type: select
#. Choices
#: ../auctex/templates:2001
msgid "Console"
msgstr "Consola"

#. Type: select
#. Choices
#: ../auctex/templates:2001
msgid "File"
msgstr "Ficheiro"

#. Type: select
#. Description
#: ../auctex/templates:2002
msgid "Parsing output destination:"
msgstr "Destino do 'output' da análise:"

#. Type: select
#. Description
#: ../auctex/templates:2002
msgid ""
"You chose to parse TeX macro packages and LaTeX style files in foreground. "
"This operation generates a lot of information. Please choose where this "
"information should be sent:"
msgstr ""
"Escolheu que a análise dos pacotes de macros do TeX e os ficheiros de estilo "
"do LaTeX seja feita em primeiro plano. Esta operação gera uma grande "
"quantidade de informação. Por favor escolha onde deve ser colocada esta "
"informação:"

#. Type: select
#. Description
#. Translators: do NOT translate ${LOGFILE}
#: ../auctex/templates:2002
msgid ""
"  Console: output goes to the current console;\n"
"  File:    output goes to ${LOGFILE}."
msgstr ""
"  Consola:  a saída do programa vai para a consola actual;\n"
"  Ficheiro: a saída do programa vai para ${LOGFILE}."

