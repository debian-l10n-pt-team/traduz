# Portuguese translation for dhis-client's debconf messages.
# Miguel Figueiredo <elmig@debianpt.org>, 2005
#
# 2005-12-27 - Miguel Figueiredo - initial translation
#
msgid ""
msgstr ""
"Project-Id-Version: dhis-client 5.3-1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-12-10 09:49-0700\n"
"PO-Revision-Date: 2005-12-27 16:27+0000\n"
"Last-Translator: Miguel Figueiredo <elmig@debianpt.org>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: note
#. Description
#: ../dhis-client.templates:3
msgid "You should register with a DHIS service provider."
msgstr "Deve registar-se num provedor de serviço DHIS."

#. Type: note
#. Description
#: ../dhis-client.templates:3
msgid ""
"If you are using the DHIS client for the first time you should read /usr/"
"share/doc/dhis-client/README.Debian for instructions on how to register with "
"a DHIS service provider and get the configuration data to make your own '/"
"etc/dhid.conf'."
msgstr ""
"Se está a utilizar o cliente de DHIS pela primeira vez deve ler /usr/share/"
"doc/dhis-client/README.Debian para saber como registar-se num provedor de "
"serviço DHIS e obter os dados de configuração para fazer o seu próprio "
"'/etc/dhid.conf'."

#. Type: note
#. Description
#: ../dhis-client.templates:11
msgid "You seem to have a 3.x/4.x config file."
msgstr "Você parece ter um ficheiro de configuração de versão 3.x/4.x."

#. Type: note
#. Description
#: ../dhis-client.templates:11
msgid ""
"You should read /usr/share/doc/dhis-client/README.Debian for instructions on "
"how to update /etc/dhid.conf."
msgstr ""
"Para instruções de como actualizar /etc/dhid.conf você deve ler "
"/usr/share/doc/dhis-client/README.Debian."
