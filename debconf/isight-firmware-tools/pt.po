# translation of isight-firmware-tools debconf to Portuguese
# Copyright (C) 2008 the isight-firmware-tools's copyright holder
# This file is distributed under the same license as the isight-firmware-tools package.
#
# Américo Monteiro <a_monteiro@netcabo.pt>, 2008.
msgid ""
msgstr ""
"Project-Id-Version: isight-firmware-tools 1.2-6\n"
"Report-Msgid-Bugs-To: isight-firmware-tools@packages.debian.org\n"
"POT-Creation-Date: 2008-11-14 18:34+0100\n"
"PO-Revision-Date: 2008-11-14 20:05+0000\n"
"Last-Translator: Américo Monteiro <a_monteiro@netcabo.pt>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Extract firmware from Apple driver?"
msgstr "Extrair o firmware da driver Apple?"

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"If you choose this option, please make sure that you have access to the "
"AppleUSBVideoSupport driver file."
msgstr ""
"Se você escolher esta opção, por favor certifique-se que tem acesso ao "
"ficheiro AppleUSBVideoSupport do driver."

#. Type: string
#. Description
#: ../templates:3001
msgid "Apple driver file location:"
msgstr "Localização do ficheiro driver Apple:"

#. Type: note
#. Description
#: ../templates:4001
#| msgid "Apple driver file location:"
msgid "Apple driver file not found"
msgstr "Ficheiro driver Apple não encontrado"

#. Type: note
#. Description
#: ../templates:4001
msgid ""
"The file you specified does not exist. The firmware extraction has been "
"aborted."
msgstr "O ficheiro que especificou não existe. A extracção do firmware foi abortada."

#. Type: text
#. Description
#: ../templates:5001
msgid "Firmware extracted successfully"
msgstr "Firmware extraído com sucesso"

#. Type: text
#. Description
#: ../templates:5001
msgid "The iSight firmware has been extracted successfully."
msgstr "O firmware iSight foi extraído com sucesso."

#. Type: text
#. Description
#: ../templates:6001
msgid "Failed to extract firmware"
msgstr "Falha ao extrair o firmware"

#. Type: text
#. Description
#: ../templates:6001
msgid ""
"The firmware extraction failed. Please check that the file you specified is "
"a valid firmware file."
msgstr ""
"A extracção do firmware falhou. Por favor verifique que o ficheiro que "
"especificou é um ficheiro de firmware válido."

