# Portuguese translation of dvi2ps's debconf messages.
# Copyright (C) 2007
# This file is distributed under the same license as the dvi2ps package.
# Ricardo Silva <ardoric@gmail.com>, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: dvi2ps 3.2j-15\n"
"Report-Msgid-Bugs-To: ohura@debian.org\n"
"POT-Creation-Date: 2006-12-04 23:07+0900\n"
"PO-Revision-Date: 2007-03-14 13:24+0000\n"
"Last-Translator: Ricardo Silva <ardoric@gmail.com>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: note
#. Description
#: ../templates:1001
msgid "Old fontdesc file backuped"
msgstr "Ficheiro fontdesc antigo salvaguardado"

#. Type: note
#. Description
#: ../templates:1001
msgid ""
"The old fontdesc could not work with this version of dvi2ps so it was "
"renamed as /etc/texmf/dvi2ps/fontdesc.disable and the new version of "
"fontdesc is installed now."
msgstr ""
"O antigo fontdesc não funciona com esta versão do dvi2ps portanto foi "
"renomeado para /etc/texmf/dvi2ps/fontdesc.disable e a nova versão do "
"fontdesc foi instalado."

#. Type: note
#. Description
#: ../templates:1001
msgid ""
"If you don't know what is fontdesc or you haven't modified it, you should or "
"might remove unnecessary fontdesc.disable file."
msgstr ""
"Se não sabe o que é o fontdesc ou não o modificou, pode e deve remover o ficheiro "
"desnecessário fontdesc.disable."

#. Type: note
#. Description
#: ../templates:1001
msgid ""
"If you had modified fontdesc then please compare fontdesc with fontdesc."
"disable and you could edit fontdesc so that dvi2ps works in the same way as "
"the previous version.  After that, you should or might remove unnecessary "
"fontdesc.disable file."
msgstr ""
"Se tiver modificado o fontdesc, por favor compare o fontdesc com o fontdesc."
"disable e pode editar o fontdesc para que o dvi2ps funcione da mesma forma "
"que com a versão antiga. Depois, pode e deve remover o ficheiro desnecessário "
"fontdesc.disable."

#. Type: note
#. Description
#: ../templates:2001
msgid "Old configk file backuped"
msgstr "Ficheiro configk antigo salvaguardado"

#. Type: note
#. Description
#: ../templates:2001
msgid ""
"The old configk could not work with the current tetex environment, so it was "
"renamed as /etc/texmf/dvi2ps/configk.disable and the new version of configk "
"is installed now."
msgstr ""
"O ficheiro configk antigo não funciona com o ambiente actual do tetex, portanto "
"foi renomeado para /etc/texmf/dvi2ps/configk.disable e a nova versão do configk "
"foi agora instalado."

#. Type: note
#. Description
#: ../templates:2001
msgid ""
"If you don't know what is configk or you haven't modified it, you should or "
"might remove unnecessary configk.disable file."
msgstr ""
"Se não sabe o que é o configk ou não o modificou, pode e deve remover o ficheiro "
"desnecessário configk.disable."

#. Type: note
#. Description
#: ../templates:2001
msgid ""
"If you had modified configk then please compare configk with configk.disable "
"and you could edit configk so that dvi2ps works in the same way as the "
"previous version.  After that, you should or might remove unnecessary "
"configk.disable file."
msgstr ""
"Se modificou o configk, por favor compare o configk com o configk.disable "
"e pode editar o configk para que o dvi2ps funcione da mesma forma que "
"antigamente. Depois, pode e deve remover o ficheiro desnecessário configk."
"disable."


