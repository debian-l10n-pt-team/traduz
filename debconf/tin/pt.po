# Portuguese translation for tin's debconf messages.
# Under the same license as the tin package.
# Luísa Lourenço <kikentai@gmail.com>
#
msgid ""
msgstr ""
"Project-Id-Version: tin\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-03-20 19:01+0100\n"
"PO-Revision-Date: 2006-08-03 14:07+0000\n"
"Last-Translator: Luísa Lourenço <kikentai@gmail.com>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../tin.templates:3
msgid "Enter the fully qualified domain name of your news server"
msgstr "Insira o nome completo e qualificado do domínio do seu servidor de news"

#. Type: string
#. Description
#: ../tin.templates:3
msgid ""
"What news server (NNTP server) should be used for reading and posting news?"
msgstr ""
"Qual o servidor de news (servidor NNTP) que deve ser usado para ler e "
"publicar news?"
