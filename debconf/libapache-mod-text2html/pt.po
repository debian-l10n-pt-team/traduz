# Portuguese translation of libapache-mod-text2html's debconf messages.
# Copyright (C) 2007
# This file is distributed under the same license as the libapache-mod-text2html package.
# Ricardo Silva <ardoric@gmail.com>, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: libapache-mod-text2html 1.0-9\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-01-13 19:38+0100\n"
"PO-Revision-Date: 2007-03-12 21:21+0000\n"
"Last-Translator: Ricardo Silva <ardoric@gmail.com>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Reload apache?"
msgstr "Carregar a nova configuração do apache?"

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"Some apache modules were upgraded. Apache has to be reloaded to use the new "
"version. I can reload it automatically for you now, or you can do it later "
"by executing /etc/init.d/apache reload"
msgstr ""

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Reload apache-ssl?"
msgstr "Carregar a nova configuração do apache-ssl"

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"Some apache modules were upgraded. Apache-ssl has to be reloaded to use the "
"new version. I can reload it automatically for you now, or you can do it "
"later by executing /etc/init.d/apache-ssl reload"
msgstr ""
"Foram actualizados alguns módulos do apache. A configuração do apache tem de ser "
"carregada para usar a nova versão. Posso carregá-la automaticamente, ou pode fazê-lo"
"mais tarde executando /etc/init.d/apache reload"

#. Type: boolean
#. Description
#: ../templates:3001
msgid "Reload apache-perl?"
msgstr "Carregar a nova configuração do apache-perl"

#. Type: boolean
#. Description
#: ../templates:3001
msgid ""
"Some apache modules were upgraded. Apache-perl has to be reloaded to use the "
"new version. I can reload it automatically for you now, or you can do it "
"later by executing /etc/init.d/apache-perl reload"
msgstr ""
"Foram actualizados alguns módulos do apache. A configuração do apache-ssl tem de ser "
"carregada para usar a nova versão. Posso carregá-la automaticamente, ou pode fazê-lo"
"mais tarde executando /etc/init.d/apache-perl reload"

#. Type: multiselect
#. Description
#: ../templates:4001
msgid "Please select new modules that ${flavour} should load"
msgstr "Por favor, escolha os novos módulos que ${flavour} deve carregar"
