# Portuguese translation of slapos.tool.format debconf messages.
# Copyright (C) 2011
# This file is distributed under the same license as the slapos.tool.format package.
# Rui Branco <ruipb@debianpt.org>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: slapos.tool.format 1.0~20110420+1.git67c5b39-2\n"
"Report-Msgid-Bugs-To: slapos.core@packages.debian.org\n"
"POT-Creation-Date: 2011-06-10 11:29+0900\n"
"PO-Revision-Date: 2011-06-13 13:23+0100\n"
"Last-Translator: Rui Branco <ruipb@debianpt.org>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"

#. Type: string
#. Description
#: ../templates:2001
msgid "SlapOS master node URL:"
msgstr "URL do nó principal do SlapOS:"

#. Type: note
#. Description
#: ../templates:3001
msgid "Master node key and certificate mandatory"
msgstr "Chave e certificado obrigatório do nó principal"

#. Type: note
#. Description
#: ../templates:3001
msgid ""
"You used an HTTPS URL for the SlapOS master node, so the corresponding "
"certificate must be placed in /etc/slapos/ssl/slapos.crt, and the key in /"
"etc/slapos/ssl/slapos.key, readable only to root."
msgstr ""
"Usou um URL HTTPS para o nó principal do SlapOS, deste modo o certificado "
"correspondente deverá ser colocado em /etc/slapos/ssl/slapos.crt, e a chave "
"em /etc/slapos/ssl/slapos.key, com acesso de leitura apenas ao root."

#. Type: string
#. Description
#: ../templates:4001
#| msgid "SlapOS client ID:"
msgid "SlapOS computer ID:"
msgstr "ID do computador SlapOS:"

#. Type: string
#. Description
#: ../templates:4001
msgid "Please specify a unique identifier for this SlapOS node."
msgstr "Por favor especifique um identificador único para este nó SlapOS."

#. Type: string
#. Description
#: ../templates:5001
msgid "Number of Computer Partitions on this computer:"
msgstr "Número de Partições de Computador neste computador:"

#. Type: string
#. Description
#: ../templates:5001
msgid ""
"A Computer Partition (CP) is an instance of a Software Release (SR). You can "
"now define how many instances will be available on this computer."
msgstr ""
"Uma 'Computer Partition' (CP) é uma instância de uma 'Software "
"Release' (SR). Pode definir o número de instâncias que estarão disponíveis "
"neste computador."

#. Type: string
#. Description
#: ../templates:5001
msgid ""
"Note that the Software Releases will be stored in /var/lib/slapos/software/, "
"whereas the Computer Partition will be stored in /var/lib/slapos/instance/."
msgstr ""
"Note que as Software Releases serão armazenadas em /var/lib/slapos/"
"software/, enquanto que a Computer Partition será armazenada em /var/lib/"
"slapos/instance/."

#. Type: string
#. Description
#: ../templates:6001
msgid "Local IPv4 network to be used for Computer Partitions:"
msgstr "Rede IPv4 local a ser usada pelas 'Computer Partitions':"

#. Type: string
#. Description
#: ../templates:6001
msgid ""
"Every Computer Partition must have an address on the same IPv4 network. "
"Please specify a network in CIDR notation (e.g.: 192.0.2.0/24)."
msgstr ""
"Todas as 'Computer Partition' necessitam de ter um endereço na mesma rede "
"IPv4. Por favor especifique uma rede com notação CIDR (p.e.: 192.0.2.0/24)."

#~ msgid "Network configuration note"
#~ msgstr "Nota de configuração da rede"

#~ msgid ""
#~ "Before using the slapformat script, you must set up a network bridge to "
#~ "be used by the Computer Partitions, and ensure IPv6 is enabled."
#~ msgstr ""
#~ "Antes de usar o 'script' em slapformat, deverá configurar uma 'bridge' de "
#~ "rede para ser usada pelas 'Computer Partitions' e garantir que IPv6 está "
#~ "activo."

#~ msgid ""
#~ "You can find more information in /usr/share/doc/slapformat/README.Debian."
#~ msgstr ""
#~ "Pode encontrar mais informação em /usr/share/doc/slapformat/README.Debian."
