# Portuguese translation of phpldapadmin's debconf messages.
# Copyright (C) 2012 The phpldapadmin's package copyright holder
# This file is distributed under the same license as the phpldapadmin package.
# Ricardo Silva <ardoric@gmail.com>, 2007.
# Miguel Figueiredo <elmig@debianpt.orgr>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: phpldapadmin 0.9.8.3-8\n"
"Report-Msgid-Bugs-To: phpldapadmin@packages.debian.org\n"
"POT-Creation-Date: 2012-02-06 12:20+0100\n"
"PO-Revision-Date: 2012-02-18 13:45+0000\n"
"Last-Translator: Miguel Figueiredo <elmig@debianpt.org>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../phpldapadmin.templates:1001
#| msgid "LDAP server host address"
msgid "LDAP server host address:"
msgstr "Endereço do servidor LDAP:"

#. Type: string
#. Description
#: ../phpldapadmin.templates:1001
msgid ""
"Please enter the host name or the address of the LDAP server you want to "
"connect to."
msgstr ""
"Por favor introduza o nome da máquina ou o endereço do servidor LDAP ao qual "
"se deseja ligar."

#. Type: boolean
#. Description
#: ../phpldapadmin.templates:2001
#| msgid "Support for ldaps protocol"
msgid "Enable support for ldaps protocol?"
msgstr "Activar suporte para o protocolo ldaps?"

#. Type: boolean
#. Description
#: ../phpldapadmin.templates:2001
msgid ""
"If your LDAP server supports TLS (Transport Security Layer), you can use the "
"ldaps protocol to connect to it."
msgstr ""
"Se o seu servidor LDAP suportar TLS (Transport Security Layer), pode utilizar "
"o protocolo ldaps para se ligar ao servidor."

#. Type: string
#. Description
#: ../phpldapadmin.templates:3001
#| msgid "Distinguished name of the search base"
msgid "Distinguished name of the search base:"
msgstr "Nome distinto (DN) da base de busca:"

#. Type: string
#. Description
#: ../phpldapadmin.templates:3001
msgid ""
"Please enter the distinguished name of the LDAP search base. Many sites use "
"the components of their domain names for this purpose. For example, the "
"domain \"example.com\" would use \"dc=example,dc=com\" as the distinguished "
"name of the search base."
msgstr ""
"Por favor introduza o nome distinto da base de busca LDAP. Muitos sites "
"utilizam componentes dos seus nomes de domínio para este propósito. Por "
"exemplo, o domínio \"exemplo.com\" utilizaria \"dc=example,dc=com\" como o "
"nome distinto da base de busca."

#. Type: select
#. Choices
#: ../phpldapadmin.templates:4001
msgid "session"
msgstr "sessão"

#. Type: select
#. Choices
#: ../phpldapadmin.templates:4001
msgid "cookie"
msgstr "cookie"

#. Type: select
#. Choices
#: ../phpldapadmin.templates:4001
msgid "config"
msgstr "config"

#. Type: select
#. Description
#: ../phpldapadmin.templates:4002
msgid "Type of authentication"
msgstr "Tipo de autenticação"

#. Type: select
#. Description
#: ../phpldapadmin.templates:4002
msgid ""
"session : You will be prompted for a login dn and a password everytime\n"
"          you connect to phpLDAPadmin, and a session variable on the\n"
"          web server will store them. It is more secure so this is the\n"
"          default."
msgstr ""
"sessão : Ser-lhe-á perguntado um login dn e uma palavra-passe sempre\n"
"         que se ligar ao phpLDAPadmin, e uma variável de sessão no \n"
"         servidor web irá guardá-las. Esta é a forma mais segura e \n"
"         por isso é a predefinição."

#. Type: select
#. Description
#: ../phpldapadmin.templates:4002
msgid ""
"cookie :  You will be prompted for a login dn and a password everytime\n"
"          you connect to phpLDAPadmin, and a cookie on your client will\n"
"          store them."
msgstr ""
"cookie : Ser-lhe-á perguntado um login dn e uma palavra-passe sempre\n"
"         que se ligar ao phpLDAPadmin, e uma cookie no seu cliente web\n"
"         irá guarda-los."

#. Type: select
#. Description
#: ../phpldapadmin.templates:4002
msgid ""
"config  : login dn and password are stored in the configuration file,\n"
"          so you have not to specify them when you connect to\n"
"          phpLDAPadmin."
msgstr ""
"configuração: o login dn e a palavra-passe são guardados no ficheiro de\n"
"              configuração, portanto não tem de os especificar quando se\n"
"              ligar ao phpLDAPadmin."

#. Type: string
#. Description
#: ../phpldapadmin.templates:5001
#| msgid "Login dn for the LDAP server"
msgid "Login dn for the LDAP server:"
msgstr "Login dn para o servidor LDAP:"

#. Type: string
#. Description
#: ../phpldapadmin.templates:5001
msgid ""
"Enter the name of the account that will be used to log in to the LDAP "
"server. If you chose a form based authentication this will be the default "
"login dn. In this case you can also leave it empty, if you do  not want a "
"default one."
msgstr ""
"Introduza o nome da conta que será usada para se autenticar no servidor "
"LDAP. Se escolher uma autenticação baseada em formulários será este o dn "
"login por omissão. Nesse caso pode também deixar em branco, se não quiser "
"um por omissão."

#. Type: string
#. Description
#: ../phpldapadmin.templates:6001
#| msgid "Login password for the LDAP server"
msgid "Login password for the LDAP server:"
msgstr "Palavra-passe para o login do servidor LDAP:"

#. Type: string
#. Description
#: ../phpldapadmin.templates:6001
msgid ""
"Enter the password that will be used to log in to the LDAP server. Note: the "
"password will be stored in clear text in config.php, which is not world-"
"readable."
msgstr ""
"Introduza a palavra-passe que irá ser usada para se autenticar no servidor "
"LDAP. Nota: a palavra-passe será guardada em claro no config.php, que não "
"tem permissões de leitura para todos os utilizadores."

#. Type: multiselect
#. Choices
#: ../phpldapadmin.templates:7001
msgid "apache2"
msgstr "apache2"

#. Type: multiselect
#. Description
#: ../phpldapadmin.templates:7002
#| msgid "Web server which will be reconfigured automatically"
msgid "Web server(s) which will be reconfigured automatically:"
msgstr "Servidor(es) web que será(ão) reconfigurado(s) automaticamente:"

#. Type: multiselect
#. Description
#: ../phpldapadmin.templates:7002
#| msgid ""
#| "phpLDAPadmin supports any web server that PHP does, but this automatic "
#| "configuration process only supports Apache."
msgid ""
"phpLDAPadmin supports any web server that PHP does, but this automatic "
"configuration process only supports Apache2."
msgstr ""
"O phpLDAPadmin suporta qualquer servidor web que o PHP suporte, mas este "
"processo automático de configuração apenas suporta o Apache2."

#. Type: boolean
#. Description
#: ../phpldapadmin.templates:8001
msgid "Should your webserver(s) be restarted?"
msgstr "Deve(m) o(s) servidor(es) web ser(em) reiniciado(s)?"

#. Type: boolean
#. Description
#: ../phpldapadmin.templates:8001
msgid ""
"Remember that in order to apply the changes your webserver(s) has/have to be "
"restarted."
msgstr ""
"Lembre-se que para as alterações serem aplicadas o(s) seu(s) servidor(es) "
"web tem/têm de ser reiniciados."

#~ msgid "session, cookie, config"
#~ msgstr "sessão, cookie, configuração"

#~ msgid "apache, apache-ssl, apache-perl, apache2"
#~ msgstr "apache, apache-ssl, apache-perl, apache2"

#~ msgid "Restart of your webserver(s)"
#~ msgstr "Reiniciar o(s) seu(s) servidor(es) web?"
