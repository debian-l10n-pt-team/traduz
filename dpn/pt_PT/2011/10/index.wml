#use wml::debian::projectnews::header PUBDATE="2011-06-24" SUMMARY="Chegada da nova actualização do Debian: 6.0.2, Novas sessões IRC de treino, Derivados do Planet Debian, instalador gráfico para o Debian GNU/kFreeBSD"

# $Id: index.wml 897 2011-07-05 23:58:35Z ruipb $
# $Rev: 897 $
# Status: [sent]

<p>Bem vindo à décima edição da DPN, o boletim informativo para a 
comunidade Debian. Os tópicos desta edição incluem:</p>
<toc-display/>

<toc-add-entry name="pointrelease">Nova actualização do Debian GNU/Linux</toc-add-entry>
<p>
O Philip Kern anunciou <a
href="http://lists.debian.org/debian-stable-announce/2011/06/msg00000.html">o 
lançamento do Debian GNU/Linux 6.0.2</a>. Esta actualização, agendada 
para Sábado, dia 25 de Junho, irá fundamentalmente adicionar correcções a problemas 
de segurança à versão estável em conjunto com alguns ajustamentos a problemas 
sérios.
</p>

<toc-add-entry name="country">Espalhando a palavra</toc-add-entry>

<p>
O Christian Perrier publicou o seu <a
href="http://www.perrier.eu.org/weblog/2011/06/12#devel-countries-201106-3">\
relatório anual sobre o número de developers por país</a>.
Entre estes números, podemos verificar que os <a
href="http://www.debian.org/devel/developers.loc">Debian
developers estão presentes</a> em cinquenta e sete países.
Foram contados mais vinte e três Debian developers activos
este ano, em conjunto com mais dois países.
</p>


<toc-add-entry name="ircsessions">Novas sessões IRC de treino</toc-add-entry>

<p>
O Projecto Debian Project iniciou uma <a href="$(HOME)/News/2011/20110617">nova série de 
Sessões IRC de treino</a>, tomarão lugar no IRC e serão dadas por membros experientes da 
comunidade.
Desta vez, existiram dois tipos diferentes de séries: não-técnicas,chamadas <q>Pergunte 
a ...</q> e mais técnicas <q>Um dia com ...</q>. 
Informação adicional e calendarização das sessões está sempre disponível na 
<a href="http://wiki.debian.org/IRC/debian-meeting">página wiki</a> relacionada.
</p>
<p>
A primeira sessão, ocorreu a 22 de Junho e foi apresentada por Stefano Zacchiroli,
actual líder do Projecto Debian, foi seguida por mais de 70 participantes que colocaram 
ao Stefano várias questões interessantes; está disponível um <a
href="http://meetbot.debian.net/debian-meeting/2011/debian-meeting.2011-06-22-19.01.log.html">log
da sessão</a>.
Na próxima sessão, agendada para 1 de Julho às 19:00 UTC, o Gregor Herrmann
irá contar-nos sobre o dia a dia de trabalho com a equipa Debian Perl e como fazer para 
juntar-se a ela. Não a percam!
</p>


<toc-add-entry name="debianedu">Relatório do encontro de developers do Debian Edu</toc-add-entry>

<p>
O Holger Levsen enviou um <a
href="http://lists.debian.org/201106141624.46478.holger@layer-acht.org">\
relatório a partir do encontro de developers do Debian Edu em Hamburgo, Alemanha.</a>
O objectivo principal deste 'sprint' foi a preparação do lançamento do Debian Edu <q>Squeeze</>.
Os developers do Debian Edu discutiram e trabalharam vários temas como a documentação,
integração Samba, e teste das imagens de instalação do <q>Squeeze</q>.
Mais de 200 commits foram enviados para o repositório subversion do Debian Edu e 
corrigidos 18 bugs (bem como uma quantidade de bugs enviados a <tt>bugs.skolelinux.org</tt>). 
Os participantes tiveram também uma longa discussão com duas pessoas do 
<q>Chaos macht Schule</q>, subprojecto CCC. No final do seu mail o Holger agradece 
ao CCC Hamburg, Opensides.be, à associação OLPC.de e ao programa Debian sprints
pelo seu suporte financeiro para o evento.
</p>


<toc-add-entry name="planetderivatives">Conheça os Derivados do Planet Debian!</toc-add-entry>

<p>
O Paul Wise <a href="http://bonedaddy.net/pabs3/log/2011/06/16/planet-debian-derivatives/">\
criou uma entrada no seu blog sobre a criação</a> de <a href="http://planet.debian.org/deriv/"><q>Derivados 
do Planet Debian</q></a> que agrega os blogues e planetas de todas as distribuições representadas no 
census dos derivados. Como disse o Paul, a sua primeira conclusão concreta surgiu do <a
href="http://wiki.debian.org/Derivatives/Census">census dos derivados Debian</a>. Ele partilhou 
os seus planos sobre a integração de informação sobre os derivados com a infraestrutura Debian, e 
agradeceu ao Jörg Jaspert <q>por efectuar os procedimentos necessários para a configuração</q>.
</p>


<toc-add-entry name="di-bpo">Instalador debian-installer com Linux 2.6.38 para o <q>Squeeze</q></toc-add-entry>

<p>
O Kenshi Muto anunciou o <a href="http://kmuto.jp/b.cgi/debian/d-i-2638-squeeze.htm">lançamento 
do debian-installer para 'backported' amd64 para Debian GNU/Linux 6.0 <q>Squeeze</q>, disponível na <a
href="http://kmuto.jp/debian/d-i/">sua página de arquivo de imagens</a>. Esta imagem é baseada no Debian 6.0.1 
e contém firmwares. O Kenshi adicionou também na imagem um novo cliente DHCP que corrige um problema 
de interactividade com alguns servidores DHCP. Note por favor que esta é imagem não oficial e que deverá 
apenas utilizá-la se realmente necessitar.
</p>


<toc-add-entry name="debconfkeys">DebConf11: Chamada de chaves para keysigning</toc-add-entry>

<p>
O Aníbal Monsalve Salazar enviou um aviso sobre a <a
href="http://lists.debian.org/debian-devel-announce/2011/06/msg00001.html">organização 
do keysigning na DebConf11</a> que deverá ocorrer em Banja Luka, Republika Sérvia,
Bósnia e Herzegovina. Todos os que tencionam participar no keysigning deverão enviar 
a sua chave pública protegida ASCII antes de 23:59 UTC de Domingo dia 10 de Julho de 2011.
Toda a informação necessária pode ser encontrada na <a
href="http://people.debian.org/~anibal/ksp-dc11/ksp-dc11.html">página correspondente</a>.
</p>


<toc-add-entry name="kfreeBSD">Instalador gráfico para Debian GNU/kFreeBSD</toc-add-entry>

<p>
O Robert Millan anunciou que <a
href="http://lists.debian.org/debian-bsd/2011/06/msg00187.html">um
instalador gráfico está agora disponível para Debian GNU/kFreeBSD</a>: com ele 
é agora possível instalar o <q>Wheezy</q> de um modo mais confortável. Pode 
encontrá-lo na <a
href="http://d-i.debian.org/daily-images/kfreebsd-amd64/daily/netboot/gtk/">página 
das imagens</a>.
</p>


<toc-add-entry name="iceweasel">Iceweasel 5.0 em <q>experimental</q></toc-add-entry>

<p>
O Mike Hommey criou uma entrada no blog <a href="http://glandium.org/blog/?p=2108"> 
adicionando o Iceweasel 5.0 ao <q>experimental</q></a>. O Mike noticiou ainda que 
descontinuou o backport do Iceweasel 4.0 para o <q>Squeeze</q>, no entanto irá suportar 
'backports patches' para o 3.5 em <q>Squeeze</q> e 3.0 em <q>Lenny</q>. No final do seu 
post acrescentou: <q>Nas próximas semanas, irão ocorrer alterações ao repositório <a
href="http://mozilla.debian.net/">mozilla.debian.net</a>, mas darei mais detalhes quando 
isso acontecer</q>.
</p>


<toc-add-entry name="twid">Entrevistas futuras</toc-add-entry>

<p>Ocorreram duas entrevistas <q>Pessoas por detrás do Debian</q>: com
<a href="http://raphaelhertzog.com/2011/06/10/people-behind-debian-philipp-kern/">Philipp
Kern</a>, Gestor do Lançamento Stable e membro da equipa wanna-build; e com 
<a href="http://raphaelhertzog.com/2011/06/24/people-behin-debian-sam-hartman-kerberos-package-maintainer/">Sam
Hartman</a>, maintainer do pacote Kerberos, que explicou como utiliza Debian apesar de ser invisual.
</p>


<toc-add-entry name="other">Outras notícias</toc-add-entry>

<p>
O Christian Perrier noticiou que o <a
href="http://www.perrier.eu.org/weblog/2011/06/10#630000">\
bug #630000</a> foi reportado sexta-feira, 10 de Junho de 2011.
</p>

<p>
O Daniel Barlow criou uma entrada no blog sobre  
<a
href="http://www.debian-administration.org/article/669/Cloning_a_Debian_system_-_identical_packages_and_versions">como
clonar um sistema Debian</a> instalando-o com pacotes e versões idênticas.
</p>

<p>
O Martin Zobel-Helas escreveu um artigo interessante sobre <a
href="http://blog.zobel.ftbfs.de/2011/06/how-you-can-help-debian-1.html">como
contribuir para Debian</a>. Ele listou varíos modos de ajudar o Debian:
desde reportar bugs e tentar reproduzi-los, escrever documentação ou traduzir a 
existente, organizar eventos ou oferecer conversas sobre tópicos relacionados 
com Debian. E mesmo que não tenha tempo para activamente trabalhar com o Debian, 
como ajudar doando dinheiro ou hardware. Como o Martin 
disse: <q>Encontre a sua área para trabalhar com Debian, e não pense que não pode 
ajudar. Mesmo designers gráficos, advogados ou eclesiásticos, por exemplo, 
podem ajudar Debian!</q>.
</p>
## Maybe it need some rewrites: the end doesn't sound really ok, to me.
## --MadameZou

<toc-add-entry name="newcontributors">Novos Debian Contributors</toc-add-entry>

	<p>
14 pessoas <a href="http://udd.debian.org/cgi-bin/new-maintainers.cgi">iniciaram 
a manutenção de pacotes </a> desde a última edição das Notícias do Projecto Debian. 
Por favor dêem as boas vindas a John T. Nogatch, Matthias Kümmerer, Jonathan Riddell, 
Alexey S Kravchenko, Cleto Martín, Petr Hlozek, Nikolaus Rath, Maxime Chatelle,
Gergely Nagy, Leonid Borisenko, Julien Dutheil, Manish Sinha, Bastian
Blywis, e Wences Arana ao nosso projecto!</p>


<toc-add-entry name="dsa">Avisos importantes da Segurança Debian</toc-add-entry>

	<p>A Equipa de Segurança da Debian lançou recentemente 
	avisos para os seguintes pacotes (entre outros):
<a href="$(HOME)/security/2011/dsa-2256">tiff</a>,
<a href="$(HOME)/security/2011/dsa-2257">vlc</a>,
<a href="$(HOME)/security/2011/dsa-2258">kolab-cyrus-imapd</a>,
<a href="$(HOME)/security/2011/dsa-2259">fex</a>,
<a href="$(HOME)/security/2011/dsa-2260">rails</a>,
<a href="$(HOME)/security/2011/dsa-2261">redmine</a>,
<a href="$(HOME)/security/2011/dsa-2262">moodle</a>,
<a href="$(HOME)/security/2011/dsa-2263">movabletype-opensource</a>,
<a href="$(HOME)/security/2011/dsa-2264">linux-2.6</a>,
<a href="$(HOME)/security/2011/dsa-2265">perl</a>,
	Por favor leia-os cuidadosamente e tome as medidas apropriadas.</p>

<p>Note por favor que estes são apenas uma selecção dos avisos de segurança mais importantes 
das últimas semanas. Se se quiser manter actualizado sobre estes avisos de segurança lançados 
pela Equipa de Segurança Debian, por favor subscreva a 
<a href="http://lists.debian.org/debian-security-announce/">lista de mail da segurança
</a> (e a separada <a href="http://lists.debian.org/debian-backports-announce/">lista de 
backports </a>, e <a href="http://lists.debian.org/debian-stable-announce/">lista de 
actualizações stable</a> ou <a href="http://lists.debian.org/debian-volatile-announce/">a lista 
volatile</a>, para a <q>Lenny</q>, a distribuição oldstable ) para avisos.</p>


<toc-add-entry name="nnwp">Pacotes novos e dignos de nota</toc-add-entry>

<p>
263 pacotes foram adicionados ao arquivo Debian unstable recentemente. 
<a href="http://packages.debian.org/unstable/main/newpkg">Entre muitos 
outros</a> estão:</p>

<ul>
<li><a href="http://packages.debian.org/unstable/main/apt-clone">apt-clone &mdash; script para criar um conjunto de estado de pacotes</a></li>
<li><a href="http://packages.debian.org/unstable/main/brltty-espeak">brltty-espeak &mdash; acesso a software para um invisual - espeak driver</a></li>
<li><a href="http://packages.debian.org/unstable/main/c2esp">c2esp &mdash; driver para impressoras a cores de jacto de tinta Kodak ESP 5xxx AiO</a></li>
<li><a href="http://packages.debian.org/unstable/main/cardstories">cardstories &mdash; jogo multi-jogador online de cartas de adivinhas</a></li>
<li><a href="http://packages.debian.org/unstable/main/clementine">clementine &mdash; reprodutor moderno de música e organizador de colecções</a></li>
<li><a href="http://packages.debian.org/unstable/main/espeakup-udeb">espeakup-udeb &mdash; configurador da voz do sintetizador de fala</a></li>
<li><a href="http://packages.debian.org/unstable/main/extundelete">extundelete &mdash; utility to recover deleted files from ext3/ext4 partition</a></li>
<li><a href="http://packages.debian.org/unstable/main/hunspell-ml">hunspell-ml &mdash; dicionário de Malaiala para o hunspell</a></li>
<li><a href="http://packages.debian.org/unstable/main/jack-stdio">jack-stdio &mdash; programa para transmitir dados áudio de e para o JACK</a></li>
<li><a href="http://packages.debian.org/unstable/main/letodms">letodms &mdash; sistema de gestão documental open source baseado em PHP e MySQL</a></li>
<li><a href="http://packages.debian.org/unstable/main/linux-headers-2.6.39-2-486">linux-headers-2.6.39-2-486 &mdash; Header files para Linux 2.6.39-2-486</a></li>
<li><a href="http://packages.debian.org/unstable/main/linux-image-2.6.39-2-686-pae">linux-image-2.6.39-2-686-pae &mdash; Linux 2.6.39 para PCs modernos</a></li>
<li><a href="http://packages.debian.org/unstable/main/m2300w">m2300w &mdash; driver para impressoras a cores laser Minolta magicolor 2300W/24000W</a></li>
<li><a href="http://packages.debian.org/unstable/main/preprocess">preprocess &mdash; preprocessador de ficheiro multi-língua</a></li>
<li><a href="http://packages.debian.org/unstable/main/rhash">rhash &mdash; utilitário para computar hash sums e links magnéticos</a></li>
<li><a href="http://packages.debian.org/unstable/main/xul-ext-linky">xul-ext-linky &mdash; extensão iceweasel para manusear links web e de imagens</a></li>
</ul>


<toc-add-entry name="wnpp">Pacotes a necessitar de trabalho</toc-add-entry>

	<p>Actualmente <a href="$(DEVEL)/wnpp/orphaned">\
306 pacotes estão órfãos</a> e <a href="$(DEVEL)/wnpp/rfa">\
142 pacotes estão prontos para adopção</a>: visite por favor a lista 
completa em <a href="$(DEVEL)/wnpp/help_requested">pacotes a necessitar 
da sua ajuda</a>.</p>


<toc-add-entry name="continuedpn">Quer continuar a ler a DPN?</toc-add-entry>

<p>Por favor ajude-nos a criar este jornal.  Continuamos a precisar de mais editores voluntários
que vigiem a comunidade Debian e ralatem sobre o que se passa. Por favor veja a 
<a href="http://wiki.debian.org/ProjectNews/HowToContribute">página de contribuição</a>
para descobrir como pode ajudar. Estamos desejosos de receber o seu mail
em <a href="mailto:debian-publicity@lists.debian.org">debian-publicity@lists.debian.org</a>.</p>


#use wml::debian::projectnews::footer editor="Francesca Ciceri, Jeremiah C. Foster, David Prévot, Alexander Reichle-Schmehl, Alexander Reshetov, Justin B Rye" translator="Rui Branco"
# Translators may also add a translator="foo, bar, baz" to the previous line
