<p>Outras páginas do BTS:</p>

<ul>
  <li><a href="./">Página principal do Sistema de Acompanhamento de Bugs (BTS).</a></li>
  <li><a href="Reporting">Instruções para reportar bugs.</a></li>
  <li><a href="Access">Acesso aos registos do sistema do BTS.</a></li>
  <li><a href="Developer">Informação para programadores (Developers) 
      sobre o BTS.</a></li>
  <li><a href="server-control">Informação para programadores (Developers) sobre
      a manipulação dos bugs através da utilzação da interface de controlo por
      email.</a></li>
  <li><a href="server-refcard">Cartão de referência do servidor de email.</a></li>
  <li><a href="server-request">Pedido de relatórios de bugs por email.</a></li>
#  <li><a href="db/ix/full.html">Full list of outstanding and recent bug
#      reports.</a></li>
#  <li><a href="db/ix/packages.html">Packages with bug reports.</a></li>
#  <li><a href="db/ix/maintainers.html">Maintainers of packages with bug
#      reports.</a></li>
</ul>
