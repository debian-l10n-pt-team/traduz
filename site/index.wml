#use wml::debian::mainpage title="O Sistema Operativo Universal"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="1.88" maintainer="Pedro Ribeiro"


<if-stable-release release="squeeze">
<span class="download"><a
href="<stable-images-url/>/multi-arch/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-i386-netinst.iso">Download
do Debian <current_release_short><em>(Instalador via rede para PC de 32 e 64 bits)</em></a> </span>
</if-stable-release>

<div id="splash">
        <h1>Debian GNU/Linux</h1>
</div>  

<div id="intro">
<p>O <a href="http://www.debian.org/">Debian</a> é um sistema operativo (SO)
<a href="intro/free">livre</a> para o seu computador. Um sistema operativo é
o conjunto de programas básicos e utilitários que fazem seu computador
funcionar. O Debian usa o kernel (núcleo de um sistema operativo),
<a href="http://www.kernel.org/">Linux</a>, mas a maior parte das ferramentas do
SO vêm do <a href="http://www.gnu.org/">projeto GNU</a>; daí o nome
GNU/Linux.</p>

<p>O Debian GNU/Linux é mais que um simples SO: vem com mais de
<packages_in_stable> <a href="distrib/packages">pacotes</a> contendo software
pré-compilado e distribuídos num formato apropriado para uma fácil instalação
na sua máquina. <a href="intro/about">Continuar...</a></p>
</div>

<hometoc/>

<p class="infobar">
A <a href="releases/stable/">última versão estável do Debian</a> é
<current_release_short>. A última atualização desta versão foi feita em
<current_release_date>. Leia mais sobre as <a href="releases/">versões
disponíveis do Debian</a>.</p>

<h2>Para Começar</h2>

<ul>
  <li>Se quiser começar a usar o Debian, pode facilmente
  <a href="distrib/">obter uma cópia</a>, e seguir as
  <a href="releases/stable/installmanual">instruções de instalação</a>
  para instalá-lo.</li>
  <li>Caso esteja a actualizar para a última versão estável a partir de uma
  versão anterior, por favor leia as <a
  href="releases/stable/releasenotes">notas de lançamento</a> antes de
  prosseguir.</li>
  <li>Para obter ajuda sobre o uso ou a configuração do Debian, veja as
  nossas páginas de <a href="doc/">documentação</a> e <a href="support">
  suporte</a>.</li>
  <li>Os utilizadores que não são falantes nativos de inglês, podem consultar a
  secção <a href="international/">internacional</a>.</li>
  <li>Quem estiver a usar uma arquitectura diferentes de Intel x86 devem conferir
  a secção de <a href="ports/">portabilidade</a>.</li>
</ul>

<hr />
<a class="rss_logo" href="News/news">RSS</a>
<h2>Notícias</h2>

<p><:= get_recent_list('News/$(CUR_YEAR)', '6', '$(ENGLISHDIR)', '', '\d+\w*' ) :></p>

<p>Para consultar notícias antigas veja a <a href="$(HOME)/News/">Página de
Notícias</a>. Se quiser ser notificado por e-mail quando aparecerem novas 
notícias sobre o Debian, assine a <a
href="MailingLists/debian-announce">lista de discussão debian-announce</a> (em
inglês).</p>

<hr>
<a class="rss_logo" href="security/dsa">RSS</a>
<h2>Alertas de Segurança</h2>

<p><:= get_recent_list ('security/2w', '10', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)' ) :></p>

<p>Para consultar alertas de segurança mais antigos veja a <a
href="$(HOME)/security/">Página de Segurança</a>.
Se quiser receber alertas de segurança assim que eles forem anunciados, assine a
<a href="http://lists.debian.org/debian-security-announce/">lista de
discussão debian-security-announce</a> (em inglês).</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
title="Notícias do Debian" href="News/news">
<link rel="alternate" type="application/rss+xml"
title="Notícias do Projeto Debian" href="News/weekly/dwn">
<link rel="alternate" type="application/rss+xml"
 title="Alertas de Segurança Debian (somente títulos)" href="security/dsa">
<link rel="alternate" type="application/rss+xml"
 title="Alertas de Segurança Debian (sumários)" href="security/dsa-long">
:#rss#}
