#use wml::debian::projectnews::header PUBDATE="2011-08-15" SUMMARY="Debian nomeada <q>Melhor Distribuição de 2011</q>, Bits da Equipa de Lançamento, Melhorias Debian GNU/kFreeBSD, Actividades de FreedomBox na DebCon11, novo mentors.debian.net"

# $Id: index.wml 909 2011-08-19 17:29:11Z ruipb $
# $Rev: 909 $
# Status: [content-frozen]

<p>Bem vindo à décima segunda edição da DPN, o boletim informativo para a 
comunidade Debian. Os tópicos desta edição incluem:</p>
<toc-display/>

<toc-add-entry name="best-distro">Debian nomeada <q>Melhor Distribuição de 2011</q> e <q>Melhor distribuição para servidores de produção</q></toc-add-entry>

<p>
O <a href="http://tuxradar.com">TuxRadar</a> recentemente <a
href="http://www.tuxradar.com/content/best-distro-2011">comparou seis das mais 
populares distribuições de Linux</a> em diferentes categorias.  Temos o prazer 
de anunciar que a Debian não só ganhou as categorias <q>Customização</q>,
<q>Comunidade</q> e <q>Performance</q>, como também o prémio global de <q>melhor 
distribuição Linux de 2011</q>!  Temos especial prazer de ter ganho na categoria 
 <q>comunidade</q>, citando TuxRadar:
<q>Há mais para a comunidade Linux que apenas números.</q>
</p>

<p>
De modo semelhante, Debian foi nomeada a <a
href="http://www.linux.com/learn/tutorials/479960:the-six-best-linux-community-server-distributions"><q>Melhor 
distribuição para servidores de produção</q></a> por Carla Schroder em <a
href="http://linux.com">linux.com</a>: <q>Tenho sido mimada pela Debian, pois 
nunca precisa de ser re-instalada e pode ser actualizada eternamente, [..] a Debian 
suporta mais pacotes que qualquer outra distribuição, deste modo é raro não 
encontrar aquilo que se procura, apenas a um passo de apt-get install</q>. Muito 
obrigado, Carla!
</p>

<toc-add-entry name="releasebits">Bits da Equipa de Lançamento</toc-add-entry>

<p>
O Neil McGovern enviou alguns <a
href="http://lists.debian.org/debian-devel-announce/2011/08/msg00000.html">Bits
a partir da equipa de lançamento</a>.
Primeiro que tudo, Neil explicou que os <q>objectivos da versão</q> consistem em 
áreas de funcionalidade em que os developers gostariam de ver como meta para o 
próximo lançamento e que deverão ser específicas, passíveis de medição, atingíveis, 
realistas, limitadas no tempo e não limitadas nos efeitos a apenas um conjunto de 
pacotes. Acrescentando, cada objectivo do lançamento, deverá ter um supervisor 
para o processo de acompanhamento do progresso. Uma lista de objectivos actuais 
está disponível na <a href="http://wiki.debian.org/ReleaseGoals/">página wiki 
relacionada</a>. Outro tópico interessante discutido no email foi o desenvolvimento 
do <q>CUT</q> (Constantly Usable Testing)(versão Teste Constantemente Utilizável) 
e de uma versão de Debian 'rolling' (rolante): a Equipa de Lançamento está séptica 
sobre a criação de uma nova suite como o modo mais eficiente de melhorar o 
processo de lançamento. De qualquer modo, certos aspectos da proposta do CUT/Rolling 
são interessantes, e assim a Equipa de Lançamento convida pessoas que tenham 
interesse a correr a suite.
O Neil falou também dos melhoramentos ao tronco (branch) <q>experimental</q> de 
Debian, de modo a evitar o abrandamento de novas funcionalidades no <q>instável</q>, 
assim como outros tópicos interessantes como a <a
href="http://lists.debian.org/debian-devel-announce/2011/03/msg00016.html">política 
do dia-0 NMU</a>, <a href="http://release.debian.org/wheezy/arch_qualify.html">
re-qualificação da arquitectura</a> e o processo de remoção de pacotes.
Para uma visão geral do trabalho da Equipa de Lançamento, pode também ver a 
gravação vídeo de <a
href="http://ftp.acc.umu.se/pub/debian-meetings/2011/debconf11/high/708_Bits_from_the_Release_Team.ogv"><q>Bits
da Equipa de Lançamento</q></a>, que ocorreu na DebConf11.
</p>


<toc-add-entry name="kbsd-improve">Recentes melhorias com Debian GNU/kFreeBSD</toc-add-entry>

<p>
O Robert Milan criou um artigo no blog <a 
href="http://robertmh.wordpress.com/2011/08/03/recent-improvements-with-debian-gnukfreebsd/">sobre
recentes melhorias</a> no 'port' Debian para o kernel FreeBSD kernel.  
Com o <a href="$(HOME)/News/2011/20110205a">lançamento do Debian 6.0 
<q>Squeeze</q></a> em Fevereiro, foi rotulado como uma <q>previsão de 
tecnologia</q>, sugerindo algumas limitações.  No entanto, muitos melhoramentos 
foram efectuados desde essa altura, incluindo o suporte para
<a href="http://lists.debian.org/debian-bsd/2011/06/msg00187.html">o instalador 
gráfico</a>,
<a href="http://packages.debian.org/search?keywords=fuse4bsd">FUSE</a>,
partições de disco encriptadas e rede wireless. É agora possível <a
href="http://wiki.debian.org/Debian_GNU/kFreeBSD_FAQ#Q._Can_I_run_Debian_GNU.2BAC8-kFreeBSD_in_a_chroot_under_FreeBSD.3F">usar
Debian GNU/kFreeBSD num chroot sobre FreeBSD</a>.
</p>

<p>Nas notícias relacionadas, o Robert <a
href="http://lists.debian.org/CAOfDtXOZwya6io6LicZ17TAezuToz2bXWMPbRXFYU9BsnHuotw@mail.gmail.com">fez 
uma chamada para utilizadores de modo a testar o suporte ZFS modificado do 
sistema de instalação.</a>
</p>

<toc-add-entry name="freedombox">Actividades FreedomBox durante a 
DebConf11</toc-add-entry>

<p>
O Bdale Garbee criou um artigo no blog sobre <a
href="http://www.gag.com/bdale/blog/posts/FreedomBox_in_Banja_Luka.html">recentes 
progressos no projecto FreedomBox</a>. Durante a DebConf11 em Banja Luka,
alguns developers do FreedomBox trabalharam juntos, corrigindo alguns problemas: 
o Bdale trabalhou no <q>freedom-maker</q>, uma conjunto de ferramentas para 
gerar imagens de software FreedomBox, que estão agora disponíveis no <a
href="http://anonscm.debian.org/gitweb/?p=freedombox/freedom-maker.git;a=summary">
repositório git do projecto</a>. O Jonas Smedegaard continuou o trabalho num 
conjunto de ferramentas de empacotamento alternativo, o <a
href="http://git.emdebian.org/?p=upstream/boxer.git;a=summary"><q>boxer</q></a>.
Agradecimentos ao Marvell, que forneceu o código fonte, o Bdale empacotou dois 
programas 'userspace' necessários para configurar e monitorizar firmware 
providenciado para o chip wireles uAP, utilizado no DreamPlug: estão agora 
disponíveis em Debian como <a
href="http://packages.debian.org/uaputl">uaputl</a> e <a
href="http://packages.debian.org/uapevent">uapevent</a>. Ainda sobre o tópico do 
DreamPlug, o Clint Adams e Jason Cooper trabalharam na adição de suporte para o 
DreamPlug de modo a enviar 'upstream' o u-boot enquanto que H&eacute;ctor Or&oacute;n 
e Nick Bane analisaram o estado actual dos 'patches' da Marvell e Globalscale 
utilizados para suportar o DreamPlug em relação ao 'upstream' e em relação às 
actuais fontes do kernel Debian. O Mirsal Ennaime trabalhou na tecnologia para a 
configuração do pacote (utilizando debconf e Config::Model), ocorreram discussões 
interessantes sobre a gestão de identidade e confiança, sumarizada na <a 
href="http://wiki.debian.org/FreedomBox/IdentityManagement"> página wiki 
relacionada</a>.
Para mais informação sobre o projecto FreedomBox, por favor visite o <a
href="http://freedomboxfoundation.org/">website oficial</a> ou veja a gravação 
video do Bdale's <a
href="http://meetings-archive.debian.net/pub/debian-meetings/2011/debconf11/low/704_FreedomBox_progress_report.ogv"><q>Relatório 
de Progresso FreedomBox</q></a>, ocorrida na DebConf11.</p>

<toc-add-entry name="mentors">Novo website para mentors.debian.net</toc-add-entry>

<p>
O Asheesh Laroia anunciou que <a
href="http://lists.debian.org/debian-devel/2011/08/msg00214.html">a nova
versão de mentors.debian.net está finalmente disponível</a>.
<a href="http://mentors.debian.net/">O novo site</a>
fornece muitas funcionalidades interessantes tais como a possibilidade de ver a 
qualidade do pacote na página do mesmo, fornecer comentários e notificações 
via email. Este site corre com código mais fácil de gerir que o anterior, é 
caracterizado por um novo visual, sendo este o mesmo do website Debian. 
O Asheesh agradece a todos os que trabalharam durante o último ano para atingir 
este resultado: Jan Dittberner, Christoph Haas,
Johnny Lamb, David Paleino, Andrey Rahmatullin, Kalle S&ouml;derman,
Christine Spang, Arno T&ouml;ll, Wolodja Wentland, Paul Wise e Serafeim Zanikolas.
</p>

<toc-add-entry name="s390x">'Port' Debian s390x</toc-add-entry>
<p>
O Aurelien Jarno criou um artigo no blog sobre o<a
href="http://blog.aurel32.net/?p=59">nascimento de um novo 'port' Debian:
<q>s390x</q></a>, a versão 64-bit do 'port' <a
href="http://www.debian.org/ports/s390/"><q>s390</q></a>.
<q>O 'port' s390 é actualmente de 31-bit a partir do ponto de vista do endereço 
(um bit é reservado para extensões de espaços de endereços de 24 a 31 bits),
deste modo cada processo é limitado a apenas 2 GB</q>, explica Aurelien.
No entanto, como hoje em dia existem aplicações que requerem mais que 2 GB
(especialmente em 'mainframes'), o novo <q>s390x</q> pode ser verdadeiramente 
útil.
O Aurelien efectuou já o 'bootstrap' da arquitectura, deste modo o construtor 
automático (autobuilder) arrancou, tendo sido já criados mais de <a
href="http://buildd.debian-ports.org/stats/graph-week-big.png"><q>65%</q></a> de 
todos os pacotes. Os maiores problemas prendem-se com a falha de pacotes actuais 
que falham na construção a partir da fonte devido a alterações do <a
href="http://lists.debian.org/debian-devel-announce/2011/02/msg00011.html">linker</a>,
<a
href="http://packages.qa.debian.org/g/gcc-4.6/news/20110627T163333Z.html">gcc-4.6</a>
e <a
href="http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=636457">curl</a>. 
Para mais informação poderá dar uma vista de olhos na <a
href="http://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=s390x;users=debian-s390@lists.debian.org">lista
de erros que bloqueiam o 'port' s390x</a>.
</p>

<toc-add-entry name="emdebian">Integrando o Emdebian Grip em Debian</toc-add-entry>

<p>
O Neil Williams enviou um <a
href="http://lists.debian.org/debian-devel/2011/08/msg00154.html">mail interessante 
relativo à possível integração de Emdebian Grip em Debian</a>.
<a href="http://www.emdebian.org/about/">Emdebian</a> é um sub-projecto oficial 
de Debian para correr Debian em dispositivos embutidos (embedded); o objectivo 
do projecto é o de fornecer pacotes mínimos de Debian com a mesma consistência que 
os regularmente oferecidos pela Debian, para serem instalados em variados tipos 
de dispositivos. Um dos Embedian 'flavours' é o <a
href="http://www.emdebian.org/grip/">Emdebian Grip</a>, que pode ser descrito 
como uma pequena distribuição compatível com Debian com pacotes optimizados.
Durante a DebConf11, várias discussões tiveram lugar de modo a integrar o Emdebian
Grip directamente dentro do arquivo Debian e no seu processo de lançamento.
A integração envolverá suites paralelas (unstable-grip,
testing-grip, stable-grip, etc) com um conjunto de pacotes restrito (e apenas 
binários). Existe também um <a
href="http://lists.debian.org/debian-devel/2011/08/msg00153.html">mail
sobre a política do Emdebian Grip</a> interessante. Para mais informações, por 
favor visite o 
<a href="http://wiki.debian.org/EmdebianIntegration">sumário detalhado</a> da 
discussão.
</p>

<toc-add-entry name="twid">Outras entrevistas</toc-add-entry>

	<p>Houve uma entrevista sobre <q>Pessoas por detrás de Debian</q> com: 
<a href="http://raphaelhertzog.com/2011/08/07/people-behind-debian-margarita-manterola/">Margarita Manterola, Membro de Debian Women
</a>.
</p>


<toc-add-entry name="other">Outras notícias</toc-add-entry>

<p>
O Kenshi Muto anunciou uma <a href="http://kmuto.jp/b.cgi/debian/d-i-2639-squeeze.htm">actualização
do debian-installer 'backported' para Debian GNU/Linux 6.0 "Squeeze"</a>. As imagens 
actualizadas estão disponíveis na sua <a href="http://kmuto.jp/debian/d-i/">página 
de arquivo de imagens</a>. Estas imagens contêm a versão do kernel 2.6.39 
(bpo.2), actualizam os controladores de disco tais como o hpsa (actualizando o 
kernel-wedge) e actualizam pacotes de firmware (como o bnx2x). As imagens estão 
disponíveis para ambas as arquitecturas<q>i386</q> e <q>amd64</q>. Por favor note 
que estas imagens não são oficiais e que apenas as deverá utilizar se realmente 
necessitar delas.
</p>


<p>
O Dominique Dumont anunciou que o <a
href="http://ddumont.wordpress.com/2011/07/24/perl6-aka-rakudo-is-available-on-debian/">Perl
6 (AKA <q>rakudo</q>) está agora disponível em Debian instável</a> (e entretanto 
foi aceite na versão de <q>teste</q>). O Dominique iniciou o esforço de 
empacotamento do <a
href="http://packages.debian.org/rakudo">Perl 6 para Debian</a> depois de 
ter ouvido uma exposição inspiradora feita por Gabor Szabo na FOSDEM, intitulada 
<a href="http://www.fosdem.org/2011/schedule/event/perl6_today"><q>Usando o 
Perl 6 nos dias de hoje</q></a>.
</p>

<p>
O Alexander Wirt anunciou que as seguintes listas de correio estão agora 
disponíveis:</p>
<ul>
<li><a
href="http://lists.debian.org/debian-user-slovenian/">debian-user-slovenian</a>
&mdash; <q>debian-user</q> em Eslovaco;</li>
<li><a
href="http://lists.debian.org/debian-sprints/">debian-sprints</a> &mdash;
Discussão e coordenação para 'sprints';</li>
<li><a
href="http://lists.debian.org/debian-dug-by/">debian-dug-by</a> &mdash;
Lista de discussão para a comunidade Debian da Bielorrussia;</li>
<li><a
href="http://lists.debian.org/debian-experimental-changes/">debian-experimental-changes</a>
&mdash; Notícias sobre pacotes enviados para a distribuição experimental, de 
'developers', buildds e <q>dak</q> (the Debian Archive
Kit).</li>
</ul>

<p>
Os resultados das eleições da <a href="http://www.spi-inc.org/">SPI</a> foram 
divulgados: Jimmy Kaplowitz, Clint Adams e Robert Brockway foram eleitos para o 
conselho da SPI. A SPI (Software in the Public Interest) é uma organização sem 
fins lucrativos fundada para auxiliar o desenvolvimento e distribuição de 
hardware e software aberto por parte de organizações; para mais informação sobre 
o que é a SPI e sobre o que faz, veja a apresentação <a
href="http://meetings-archive.debian.net/pub/debian-meetings/2011/debconf11/high/703_SPI_BOF.ogv">SPI
BoF</a>, ocorrida durante a última DebConf.
</p>

<p>
O Ansgar Burchardt anunciou que <a
href="http://lists.debian.org/debian-devel-announce/2011/08/msg00001.html">o 
arquivo Debian suporta agora compressão xz</a> para ambos os formatos de pacotes 
em fonte ou binários. No entanto, os pacotes do sistema base (p.e pacotes com 
<q>Priority: required</q>) e suas dependências têm que usar o gzip caso contrário 
o debootstrap será incapaz de instalar um sistema.
</p>

<p>
O Jaldhar Harshad Vyas enviou uma actualização sobre <a
href="http://www.braincells.com/debian/index.cgi/search/item=271">o estado do 
'port' GNU/Minix</a>. A grande novidade é o sucesso da portabilidade do dpkg, 
enquanto que o 'bootstrapping' inicial é muito difícil devido a dependências 
circulares; apesar disso o Jaldhar informou que uma versão pré-alpha será 
distribuída dentro de um mês.
</p>

<p>
O Aigars Mahinovs <a
href="http://www.aigarius.com/blog/2011/08/08/debconf-t-shirts/">publicou
uma imagem interessante</a>, tirada em Banja Luka durante a DebConf11, com 
pessoas usando a t-shirt oficial da DebConf desde a DebConf3 até à DebConf11.
</p>

<p>
O Thom Holwerda criou um artigo no blog: <a
href="http://www.osnews.com/story/25041/AmigaOne_X1000_To_Ship_to_Beta_Testers_Next_Week">AmigaOne
X1000 enviado para utilizadores de teste</a>, que será entregue com uma motherboard 
<q>Nemo</q> montada pela Varisys, uma empresa Britânica. As boas notícias para os 
utilizadores Debian é que a Varisys tem a correr Debian 6.0 <q>Squeeze</q> na placa 
<q>Nemo</q>.
</p>

<toc-add-entry name="newcontributors">Novos Debian Contributors</toc-add-entry>

	<p>
2 aplicações foram 
<a href="https://nm.debian.org/nmlist.php#newmaint">aceites</a>
	como Debian Developers,
7 aplicações foram 
<a href="http://lists.debian.org/debian-project/2011/08/msg00017.html">aceites</a>
	como Debian Maintainers, e
34 pessoas <a href="http://udd.debian.org/cgi-bin/new-maintainers.cgi">iniciaram
        a manutenção de pacotes</a> desde o último número da DPN. Por favor dêem 
	as boas vindas a 
	Timo Lindfors, Cristian Greco, S&eacute;bastien Villemot,
	Ruben Molina, Philipp Kaluza, Steve Conklin, Allison Randal,
	Miguel Landaeta, John Paul Adrian Glaubit, Mario Limonciello,
	Thadeu Lima de Souza Cascardo, J&eacute;r&ocirc;me Sonrier,
	Sebastian Krzyszkowiak, Dave Walker, Sebastian Tennant,
	Julien Vaubourg, Laszlo Kajan, Peter Bennett, Karol M. Langner,
	Zhi Li, Nick Bane, Stefan Denker, Matthias Klumpp, Olaf Dietsche, 
	Wolodja Wentland, Andy Spencer, Intri Geri, Arno Onken, Harlan
	Lieberman-Berg, Florian Reitmeir, Ben Webb, George Gesslein II, 
	Melvin Winstr&oslash;m-M&oslash;ller, Pirmin Kalberer, Muneeb Shaikh, Godfrey
	Chung, Olivier Girondel, Martin Ueding, Werner Jaeger, Julia
	Palandri, Karolina Kalic, Enas Giovanni, e Michael Wild
	ao nosso projecto!</p>


<toc-add-entry name="dsa">Avisos importantes de Segurança Debian</toc-add-entry>

	<p>A Equipa de Segurança da Debian lançou recentemente 
	avisos para os seguintes pacotes (entre outros):
<a href="$(HOME)/security/2011/dsa-2282">qemu-kvm</a>,
<a href="$(HOME)/security/2011/dsa-2283">krb5-appl</a>,
<a href="$(HOME)/security/2011/dsa-2284">opensaml2</a>,
<a href="$(HOME)/security/2011/dsa-2285">mapserver</a>,
<a href="$(HOME)/security/2011/dsa-2286">phpmyadmin</a>,
<a href="$(HOME)/security/2011/dsa-2287">libpng</a>,
<a href="$(HOME)/security/2011/dsa-2288">libsndfile</a>,
<a href="$(HOME)/security/2011/dsa-2289">typo3-src</a>,
<a href="$(HOME)/security/2011/dsa-2290">samba</a>,
<a href="$(HOME)/security/2011/dsa-2291">squirrelmail</a>,
<a href="$(HOME)/security/2011/dsa-2292">isc-dhcp</a>,
<a href="$(HOME)/security/2011/dsa-2293">libxfont</a>, and
<a href="$(HOME)/security/2011/dsa-2294">freetype</a>.

	Por favor leia-os cuidadosamente e tome as medidas apropriadas.</p>

        <p>Avisos lançados para os seguintes pacotes pela equipa Debian's Backports:
<a href="http://lists.debian.org/20110718091230.GQ28896@aenima">libapache2-mod-authnz-external</a>,
<a href="http://lists.debian.org/87vcubyx97.fsf@windlord.stanford.edu">xml-security-c</a>,
<a href="http://lists.debian.org/87mxfnyx0l.fsf@windlord.stanford.edu">opensaml2</a>.
        Por favor leia-os cuidadosamente e tome as medidas apropriadas.</p>

        <p>Anúncios de actualizações para os seguintes pacotes pela equipa de lançamento Debian's Estável:
<a href="http://lists.debian.org/1312145877.2999.136.camel@hathi.jungle.funky-badger.org">clamav</a>,
<a href="http://lists.debian.org/1312839064.26351.212.camel@hathi.jungle.funky-badger.org">clive</a>.
        Por favor leia-os cuidadosamente e tome as medidas apropriadas.</p>

        <p>Anúncio de actualização para o seguinte pacote pela equipa Debian Volátil:
<a href="http://lists.debian.org/1312145785.2999.133.camel@hathi.jungle.funky-badger.org">clamav</a>.
        Por favor leia-os cuidadosamente e tome as medidas apropriadas.</p>


<p>Note por favor que estes são apenas uma selecção dos avisos de segurança mais 
importantes das últimas semanas. Se se quiser manter actualizado sobre estes 
avisos de segurança lançados pela Equipa de Segurança Debian, por favor subscreva 
a <a href="http://lists.debian.org/debian-security-announce/">lista de mail da 
segurança</a> (e a lista separada <a href="http://lists.debian.org/debian-backports-announce/">lista de 
backports</a>, e <a href="http://lists.debian.org/debian-stable-announce/">lista de 
actualizações estáveis</a> ou <a href="http://lists.debian.org/debian-volatile-announce/">lista 
da actualizações da volátil</a>, para <q>Lenny</q>, a antiga distribuição estável) para avisos.</p>


<toc-add-entry name="nnwp">Pacotes novos e dignos de nota</toc-add-entry>

<p>
780 pacotes foram adicionados ao arquivo Debian instável recentemente. 
<a href="http://packages.debian.org/unstable/main/newpkg">Entre muitos 
outros</a> estão:</p>

<ul>
<li><a href="http://packages.debian.org/unstable/main/alice">alice &mdash; Navegador Web (WebKit ou Gecko) baseado em cliente IRC</a></li>
<li><a href="http://packages.debian.org/unstable/main/collectl">collectl &mdash; Utilitário para recolher data de performance Linux</a></li>
<li><a href="http://packages.debian.org/unstable/main/getdata">getdata &mdash; gestão de bases de dados externas</a></li>
<li><a href="http://packages.debian.org/unstable/main/gnome-split">gnome-split &mdash; GNOME Split &mdash; Divisor de ficheiros para o desktop GNOME</a></li>
<li><a href="http://packages.debian.org/unstable/main/grml-rescueboot">grml-rescueboot &mdash; Integra o arranque por ISO no grub</a></li>
<li><a href="http://packages.debian.org/unstable/main/gtimer">gtimer &mdash; Temporizador de tarefas baseado em GTK</a></li>
<li><a href="http://packages.debian.org/unstable/main/jscribble">jscribble &mdash; bloco de notas gráfico para utilizar com uma pen tablet</a></li>
<li><a href="http://packages.debian.org/unstable/main/kindleclip">kindleclip &mdash;interface de utilizador para gerir ficheiros Amazon Kindle's "My Clippings"</a></li>
<li><a href="http://packages.debian.org/unstable/main/lame">lame &mdash; Biblioteca de codificação MP3 (frontend)</a></li>
<li><a href="http://packages.debian.org/unstable/main/landell">landell &mdash; gestor de streams audio e video</a></li>
<li><a href="http://packages.debian.org/unstable/main/lightdm">lightdm &mdash; simple display manager</a></li>
<li><a href="http://packages.debian.org/unstable/main/spacezero">spacezero &mdash; Jogo em rede de combate espacial de estratégia em 3D.</a></li>
<li><a href="http://packages.debian.org/unstable/main/thunar-vcs-plugin">thunar-vcs-plugin &mdash; Plugin VCS para o gestor de ficheiros Thunar</a></li>
<li><a href="http://packages.debian.org/unstable/main/tty-clock">tty-clock &mdash; Relógio simples de terminal</a></li>
<li><a href="http://packages.debian.org/unstable/main/wizznic">wizznic &mdash; Implementação do clássico arcade Puzznic</a></li>
<li><a href="http://packages.debian.org/unstable/main/xul-ext-autofill-forms">xul-ext-autofill-forms &mdash; Add-on Iceweasel/Firefox que lhe permite preencher formulários web mais rapidamente</a></li>
</ul>


<toc-add-entry name="wnpp">Pacotes a necessitar de trabalho</toc-add-entry>

	<p>Actualmente <a href="$(DEVEL)/wnpp/orphaned">
261 pacotes estão órfãos</a> e <a href="$(DEVEL)/wnpp/rfa">
132 pacotes estão prontos para adopção</a>: visite por favor a lista 
completa em <a href="$(DEVEL)/wnpp/help_requested">pacotes a necessitar 
da sua ajuda</a>.</p>


<toc-add-entry name="continuedpn">Quer continuar a ler a DPN?</toc-add-entry>

<p>Por favor ajude-nos a criar este jornal.  Continuamos a precisar de mais editores voluntários
que vigiem a comunidade Debian e relatem sobre o que se passa. Por favor veja a 
<a href="http://wiki.debian.org/ProjectNews/HowToContribute">página de contribuição</a>
para descobrir como pode ajudar. Estamos desejosos de receber o seu mail
em <a href="mailto:debian-publicity@lists.debian.org">debian-publicity@lists.debian.org</a>.</p>


#use wml::debian::projectnews::footer editor="XXX, Francesca Ciceri, Jeremiah C. Foster, David Prévot, Alexander Reichle-Schmehl, Alexander Reshetov, Justin B Rye" translator="Rui Branco"
# Translators may also add a translator="foo, bar, baz" to the previous line
