# Translation of debian-edu-config to European Portuguese
# Copyright (C) 2010 Free Software Foundation, Inc.
# This file is distributed under the same license as the debian-edu-config package.
#
# Américo Monteiro <a_monteiro@gmx.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: debian_edu 1.718\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-03-15 08:02+0100\n"
"PO-Revision-Date: 2014-08-09 00:10+0100\n"
"Last-Translator: Américo Monteiro <a_monteiro@gmx.com>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bits\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 1.4\n"

# type: Attribute 'lang' of: <html>
#. type: Attribute 'content' of: <html><head><meta>
#: index.html.en:4 index.html.en:25
msgid "en"
msgstr "pt"

# type: Content of: <html><head><title>
#. type: Content of: <html><head><title>
#: index.html.en:6
msgid "Welcome to &laquo;www&raquo;: Info page for a Debian Edu installation"
msgstr ""
"Bem vindo a &laquo;www&raquo;: Página de informações para uma instalação "
"de Debian-Edu"

# type: Attribute 'title' of: <html><head><link>
#. type: Attribute 'title' of: <html><head><link>
#: index.html.en:9
msgid "norsk"
msgstr "Norueguês"

# type: Attribute 'title' of: <html><head><link>
#. type: Attribute 'title' of: <html><head><link>
#: index.html.en:10
msgid "English"
msgstr "Inglês"

#. type: Attribute 'title' of: <html><head><link>
#: index.html.en:11
msgid "dansk"
msgstr "Dinamarquês"

# type: Attribute 'title' of: <html><head><link>
#. type: Attribute 'title' of: <html><head><link>
#: index.html.en:12
msgid "Deutsch"
msgstr "Alemão"

# type: Attribute 'title' of: <html><head><link>
#. type: Attribute 'title' of: <html><head><link>
#: index.html.en:13
msgid "français"
msgstr "Francês"

# type: Attribute 'title' of: <html><head><link>
#. type: Attribute 'title' of: <html><head><link>
#: index.html.en:14
msgid "català"
msgstr "catalão"

# type: Attribute 'title' of: <html><head><link>
#. type: Attribute 'title' of: <html><head><link>
#: index.html.en:15
msgid "español"
msgstr "espanhol"

#. type: Attribute 'title' of: <html><head><link>
#: index.html.en:16
msgid "Indonesia"
msgstr "Indonésio"

# type: Attribute 'title' of: <html><head><link>
#. type: Attribute 'title' of: <html><head><link>
#: index.html.en:17
msgid "Italiano"
msgstr "Italiano"

#. type: Attribute 'title' of: <html><head><link>
#: index.html.en:18
msgid "日本語"
msgstr "日本語"

# type: Attribute 'title' of: <html><head><link>
#. type: Attribute 'title' of: <html><head><link>
#: index.html.en:19
msgid "Nederlands"
msgstr "Holandês"

# type: Attribute 'title' of: <html><head><link>
#. type: Attribute 'title' of: <html><head><link>
#: index.html.en:20
msgid "Português"
msgstr "Português"

#. type: Attribute 'title' of: <html><head><link>
#: index.html.en:21
msgid "Română"
msgstr "Română"

# type: Attribute 'title' of: <html><head><link>
#. type: Attribute 'title' of: <html><head><link>
#: index.html.en:22
msgid "Русский"
msgstr "Русский"

#. type: Attribute 'title' of: <html><head><link>
#: index.html.en:23
msgid "中文"
msgstr "中文"

#. type: Attribute 'content' of: <html><head><meta>
#: index.html.en:24
msgid "text/html; charset=utf-8"
msgstr "text/html; charset=utf-8"

#. type: Attribute 'content' of: <html><head><meta>
#: index.html.en:26
msgid "Ole Aamot, Petter Reinholdtsen, Gaute Hvoslef Kvalnes, Frode Jemtland"
msgstr "Ole Aamot, Petter Reinholdtsen, Gaute Hvoslef Kvalnes, Frode Jemtland"

# type: Content of: <html><body><div><h2>
#. type: Content of: <html><body><div><h2>
#: index.html.en:36
msgid "Local services"
msgstr "Serviços Locais"

# type: Content of: <html><body><div><ul><li>
#. type: Content of: <html><body><div><ul><li>
#: index.html.en:38
msgid "<a href=\"/debian-edu-doc/en/\">Documentation</a>"
msgstr "<a href=\"/debian-edu-doc/en/\">Documentação</a>"

# type: Content of: <html><body><div><ul><li>
#. type: Content of: <html><body><div><ul><li>
#: index.html.en:39
msgid "<a href=\"https://www/gosa/\">GOsa<sup>2</sup> LDAP administration</a>"
msgstr ""
"<a href=\"https://www/gosa/\">GOsa<sup>2</sup> Administração do LDAP</a>"

# type: Content of: <html><body><div><ul><li>
#. type: Content of: <html><body><div><ul><li>
#: index.html.en:40
msgid "<a href=\"https://www:631/\">Printer administration</a>"
msgstr "<a href=\"https://www:631/\">Administração de Impressoras</a>"

# type: Content of: <html><body><div><ul><li>
#. type: Content of: <html><body><div><ul><li>
#: index.html.en:41
msgid "<a href=\"https://backup/slbackup-php\">Backup</a>"
msgstr "<a href=\"https://backup/slbackup-php\">Salvaguarda</a>"

# type: Content of: <html><body><div><ul><li>
#. type: Content of: <html><body><div><ul><li>
#: index.html.en:42
msgid "<a href=\"https://www/nagios3/\">Nagios</a>"
msgstr "<a href=\"https://www/nagios3/\">Nagios</a>"

# type: Content of: <html><body><div><ul><li>
#. type: Content of: <html><body><div><ul><li>
#: index.html.en:43
msgid "<a href=\"/munin/\">Munin</a>"
msgstr "<a href=\"/munin/\">Munin</a>"

# type: Content of: <html><body><div><ul><li>
#. type: Content of: <html><body><div><ul><li>
#: index.html.en:44
msgid "<a href=\"http://www/sitesummary/\">Sitesummary</a>"
msgstr "<a href=\"http://www/sitesummary/\">Sumário do Site</a>"

# type: Content of: <html><body><div><h2>
#. type: Content of: <html><body><div><h2>
#: index.html.en:47
msgid "Debian Edu"
msgstr "Debian Edu"

# type: Content of: <html><body><div><ul><li>
#. type: Content of: <html><body><div><ul><li>
#: index.html.en:49
msgid "<a href=\"http://www.skolelinux.org/\">Web page</a>"
msgstr "<a href=\"http://www.skolelinux.org/\">Página Web</a>"

# type: Content of: <html><body><div><ul><li>
#. type: Content of: <html><body><div><ul><li>
#: index.html.en:50
msgid "<a href=\"http://wiki.debian.org/DebianEdu\">Wiki page</a>"
msgstr "<a href=\"http://wiki.debian.org/DebianEdu\">Página Wiki</a>"

# type: Content of: <html><body><div><ul><li>
#. type: Content of: <html><body><div><ul><li>
#: index.html.en:51
msgid ""
"<a href=\"http://wiki.debian.org/DebianEdu/MailingLists\">Email lists</a>"
msgstr ""
"<a href=\"http://wiki.debian.org/DebianEdu/MailingLists\">Listas de Email</a>"

# type: Content of: <html><body><div><ul><li>
#. type: Content of: <html><body><div><ul><li>
#: index.html.en:52
msgid "<a href=\"http://popcon.skolelinux.org/\">Collected package usage</a>"
msgstr ""
"<a href=\"http://popcon.skolelinux.org/\">Utilização de pacotes recolhida</a>"

# type: Content of: <html><body><div><ul><li>
#. type: Content of: <html><body><div><ul><li>
#: index.html.en:53
msgid "<a href=\"http://linuxsignpost.org/\">The Linux Signpost</a>"
msgstr "<a href=\"http://linuxsignpost.org/\">O Signpost de Linux</a>"

#. type: Content of: <html><body><div><ul><li>
#: index.html.en:54
msgid ""
"<a href=\"http://www.linuxiskolen.no/slxdebianlabs/donations.html\">Donate "
"to the project</a>"
msgstr ""
"<a href=\"http://www.linuxiskolen.no/slxdebianlabs/donations.html\">Doações "
"ao projecto</a>"

# type: Content of: <html><body><div><div>
#.  Note to translators: these strings should not be translated
#. type: Content of: <html><body><div><div>
#: index.html.en:63
msgid ""
"<a href=\"index.html.ca\">[català]</a> <a href=\"index.html.da\">[dansk]</a> "
"<a href=\"index.html.de\">[Deutsch]</a> <a href=\"index.html.en\">[English]</"
"a> <a href=\"index.html.es\">[español]</a> <a href=\"index.html.fr\">"
"[français]</a> <a href=\"index.html.id\">[Indonesia]</a> <a href=\"index."
"html.it\">[Italiano]</a> <a href=\"index.html.nb\">[norsk]</a> <a href="
"\"index.html.nl\">[Nederlands]</a> <a href=\"index.html.pt\">[Português]</a> "
"<a href=\"index.html.ro\">[Română]</a> <a href=\"index.html.ru\">[Русский]</"
"a> <a href=\"index.html.zh\">[中文]</a> <a href=\"index.html.ja\">[日本語]</"
"a>"
msgstr ""
"<a href=\"index.html.ca\">[Catalão]</a> "
"<a href=\"index.html.da\">[Dinamarquês]</a> "
"<a href=\"index.html.de\">[Alemão]</a> <a href=\"index.html.en\">[Inglês]</a> "
"<a href=\"index.html.es\">[Espanhol]</a> "
"<a href=\"index.html.fr\">[Francês]</a> "
"<a href=\"index.html.id\">[Indonésio]</a> "
"<a href=\"index.html.it\">[Italiano]</a> "
"<a href=\"index.html.nb\">[norueguês]</a> "
"<a href=\"index.html.nl\">[Holandês]</a> "
"<a href=\"index.html.pt\">[Português]</a> "
"<a href=\"index.html.ro\">[Română]</a> "
"<a href=\"index.html.ru\">[Русский]</a> "
"<a href=\"index.html.zh\">[中文]</a> "
"<a href=\"index.html.ja\">[日本語]</a>"

# type: Content of: <html><body><div><h1><a>
#. type: Content of: <html><body><div><h1><a>
#: index.html.en:81
msgid "<a name=\"top\">"
msgstr "<a name=\"top\">"

# type: Attribute 'alt' of: <html><body><div><h1><a><img>
#. type: Attribute 'alt' of: <html><body><div><h1><a><img>
#: index.html.en:81
msgid "Skolelinux"
msgstr "Skolelinux"

# type: Content of: <html><body><div><h1>
#. type: Content of: <html><body><div><h1>
#: index.html.en:81
msgid "</a>"
msgstr "</a>"

#. type: Content of: <html><body><div><h2>
#: index.html.en:83
msgid "Welcome to Debian Edu / Skolelinux"
msgstr "Bem vindo ao Debian Edu / Skolelinux"

# type: Content of: <html><body><div><p>
#. type: Content of: <html><body><div><p>
#: index.html.en:84
msgid ""
"<strong>If you can see this, it means the installation of your Debian Edu "
"server was successful. Congratulations, and welcome. To change the content "
"of this page, edit /etc/debian-edu/www/index.html.en, in your favorite "
"editor.</strong>"
msgstr ""
"<strong>Se você consegue ver isto, significa que a instalação do seu "
"servidor Debian-edu teve sucesso. Parabéns e vem vindo. Para mudar o "
"conteúdo desta página, edite /etc/debian-edu/www/index.html.pt, com o seu "
"editor favorito.</strong>"

# type: Content of: <html><body><div><p>
#. type: Content of: <html><body><div><p>
#: index.html.en:89
msgid ""
"On the right side for this page you see some links that can be helpful for "
"you in your work, administrating a Debian Edu network."
msgstr ""
"No lado direito desta página você vê alguns links que podem ser úteis para "
"si e para o seu trabalho, administrando uma rede Debian Edu."

#. type: Content of: <html><body><div><ul><li>
#: index.html.en:93
msgid ""
"The links under Local services are links to services running on this "
"server.  These tools can assist you in your daily work with the Debian Edu "
"solution."
msgstr ""
"Os links sob serviços locais são links para serviços em execução neste "
"servidor. Estas ferramentas podem ajudá-lo no seu trabalho diário com "
"a solução Debian Edu."

# type: Content of: <html><body><div><p><ul><li>
#. type: Content of: <html><body><div><ul><li>
#: index.html.en:96
msgid ""
"The links under Debian Edu are links to the Debian Edu and/or Skolelinux "
"pages on the Internet."
msgstr ""
"Os links sob Debian-Edu são links para páginas do Debian-Edu e/ou do "
"Skolelinux na Internet."

# type: Content of: <html><body><div><p><ul><ul><li>
#. type: Content of: <html><body><div><ul><li><ul><li>
#: index.html.en:98
msgid ""
"<strong>Documentation:</strong> Choose this to browse the installed "
"documentation"
msgstr ""
"<strong>Documentação:</strong> Escolha isto para explorar a documentação "
"instalada."

#. type: Content of: <html><body><div><ul><li><ul><li>
#: index.html.en:99
msgid ""
"<strong>GOsa<sup>2</sup> LDAP administration:</strong> Choose this to get to "
"the GOsa<sup>2</sup> LDAP administration web system.  Use this to add and "
"edit users and machines."
msgstr ""
"<strong>GOsa<sup>2</sup> Administração de LDAP:</strong> Escolha isto para "
"ir para GOsa<sup>2</sup> Sistema web de administração LDAP. Use isto para "
"adicionar e editar utilizadores e máquinas."

# type: Content of: <html><body><div><p><ul><ul><li>
#. type: Content of: <html><body><div><ul><li><ul><li>
#: index.html.en:100
msgid ""
"<strong>Printer administration:</strong> Choose this to administer your "
"printers."
msgstr ""
"<strong>Administração de Impressora:</strong> Escolha isto para administrar "
"as suas impressoras."

# type: Content of: <html><body><div><p><ul><ul><li>
#. type: Content of: <html><body><div><ul><li><ul><li>
#: index.html.en:101
msgid ""
"<strong>Backup:</strong> Choose this to get to the backup system, here you "
"can restore or change the nightly backup"
msgstr ""
"<strong>Salvaguarda:</strong> Escolha isto para obter o sistema de "
"salvaguarda, aqui você pode restaurar ou alterar a salvaguarda (backup) "
"nocturna."

#. type: Content of: <html><body><div><ul><li><ul><li>
#: index.html.en:102
msgid ""
"<strong>Nagios:</strong> Choose this to get to the Nagios system monitor "
"pages.  The predefined user name is &laquo;nagiosadmin&raquo; and if you "
"want to change the predefined password (as root) you can run &laquo;"
"htpasswd /etc/nagios3/htpasswd.users nagiosadmin&raquo;."
msgstr ""
"<strong>Nagios:</strong> Escolha isto para chegar às páginas de "
"monitorização do sistema. O nome de utilizador predefinido é "
"&laquo;nagiosadmin&raquo; e se você quiser alterar a palavra-passe "
"predefinida (como root) você pode executar &laquo;"
"htpasswd /etc/nagios3/htpasswd.users nagiosadmin&raquo;."

# type: Content of: <html><body><div><p><ul><ul><li>
#. type: Content of: <html><body><div><ul><li><ul><li>
#: index.html.en:103
msgid ""
"<strong>Munin:</strong> Choose this to get to the Munin statistic pages."
msgstr ""
"<strong>Munin:</strong> Escolha isto para chagar ás páginas de estatísticas "
"Munin."

# type: Content of: <html><body><div><p><ul><ul><li>
#. type: Content of: <html><body><div><ul><li><ul><li>
#: index.html.en:104
msgid ""
"<strong>Sitesummary:</strong> Choose this to get to the summary report of "
"Debian Edu network machines."
msgstr ""
"<strong>Sumário do Site</strong> Escolha isto para chegar ao relatório "
"sumário das máquinas de rede de Debian Edu."

# type: Content of: <html><body><div><h2>
#. type: Content of: <html><body><div><h2>
#: index.html.en:108
msgid "Personal web pages for the users on the system."
msgstr "Páginas web pessoais para os utilizadores do sistema."

# type: Content of: <html><body><div><p>
#. type: Content of: <html><body><div><p>
#: index.html.en:109
msgid ""
"All users on the system, can create a catalog on their home directory with "
"the name &laquo;public_html&raquo;. Here the users can add their own home "
"pages. The home pages will be available at the address http://www/"
"~username/. If you have a user named Jon Doe, with the user name jond, his "
"web pages will be available at <a href=\"http://www/~jond/\">http://www/"
"~jond/</a>. If the user doesn't exist, or the catalog public_html do not "
"exist, you will get the &laquo;Not Found&raquo; error page. If the "
"public_html catalog exists, but it is empty, you will get the &laquo;"
"Permission Denied&raquo; error page. To change this, create the index.html "
"file inside the public_html catalog."
msgstr ""
"Todos os utilizadores no sistema, podem criar um catálogo no seu directório "
"home com o nome &laquo;public_html&raquo;. Aqui os utilizadores podem "
"adicionar as suas páginas pessoais. As páginas pessoais estarão disponíveis "
"no endereço http://www/~nome-utilizador/. Se você tiver um utilizador "
"chamado Jon Doe, com o nome de utilizador jond, a sua página web estará "
"disponível em  <a href=\"http://www/~jond/\">http://www/~jond/</a>. Se o "
"utilizador não existir, ou se o catálogo public_html não existir, você irá "
"obter a página de erro &laquo;Not Found&raquo;. Se o catálogo public_html "
"existir, mas estiver vazio, você vai obter a página de erro "
"&laquo;Permission Denied&raquo;. Para mudar isto, altere o ficheiro "
"index.html dentro do catálogo public_html."


