# Translation of camomile's manpage to European Portuguese
# Copyright (C) 2014 Free Software Foundation, Inc.
# This file is distributed under the same license as the camomile package.
#
# Américo Monteiro <a_monteiro@gmx.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: camomile 0.8.4-4\n"
"POT-Creation-Date: 2011-07-29 19:24+0300\n"
"PO-Revision-Date: 2014-08-14 12:50+0100\n"
"Last-Translator: Américo Monteiro <a_monteiro@gmx.com>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 1.4\n"

#. type: Content of the debian entity
#: debian/xml-man/en/camomilecharmap.xml:4 debian/xml-man/en/camomilelocaledef.xml:4 debian/xml-man/en/license.xml:4
msgid "Debian GNU/Linux"
msgstr "Debian GNU/Linux"

#. type: Content of the dhprg entity
#: debian/xml-man/en/camomilecharmap.xml:5
msgid "<command>camomilecharmap</command>"
msgstr "<command>camomilecharmap</command>"

#. type: Content of the camomile entity
#: debian/xml-man/en/camomilecharmap.xml:6 debian/xml-man/en/camomilelocaledef.xml:6
msgid "camomile"
msgstr "camomile"

#. type: Content of: <refentry><refmeta><refentrytitle>
#: debian/xml-man/en/camomilecharmap.xml:39
msgid "CAMOMILECHARMAP"
msgstr "CAMOMILECHARMAP"

#. type: Content of: <refentry><refmeta><manvolnum>
#: debian/xml-man/en/camomilecharmap.xml:40 debian/xml-man/en/camomilelocaledef.xml:40
msgid "1"
msgstr "1"

#. type: Content of: <refentry><refnamediv><refname>
#: debian/xml-man/en/camomilecharmap.xml:44 debian/xml-man/en/camomilelocaledef.xml:44
msgid "&dhprg;"
msgstr "&dhprg;"

#. type: Content of: <refentry><refnamediv><refpurpose>
#: debian/xml-man/en/camomilecharmap.xml:46
msgid "a charmap database translators for &camomile;."
msgstr "um tradutor de base de dados charmap para o &camomile;."

#. type: Content of: <refentry><refsynopsisdiv><cmdsynopsis>
#: debian/xml-man/en/camomilecharmap.xml:51
msgid ""
"&dhprg; <arg choice=\"opt\">-d <arg choice=\"req\"> "
"<replaceable>dir</replaceable> </arg> </arg> <group> <arg "
"choice=\"plain\">-help</arg> <arg choice=\"plain\">--help</arg> </group> "
"<arg choice=\"opt\"> <replaceable>file</replaceable> </arg>"
msgstr ""
"&dhprg; <arg choice=\"opt\">-d <arg choice=\"req\"> "
"<replaceable>directório</replaceable> </arg> </arg> <group> <arg "
"choice=\"plain\">-help</arg> <arg choice=\"plain\">--help</arg> </group> "
"<arg choice=\"opt\"> <replaceable>ficheiro</replaceable> </arg>"

#. type: Content of: <refentry><refsect1><title>
#: debian/xml-man/en/camomilecharmap.xml:68 debian/xml-man/en/camomilelocaledef.xml:73
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Content of: <refentry><refsect1><para>
#: debian/xml-man/en/camomilecharmap.xml:70 debian/xml-man/en/camomilelocaledef.xml:75
msgid "This manual page documents briefly the &dhprg; command."
msgstr "Este manual documenta brevemente o comando &dhprg;."

#. type: Content of: <refentry><refsect1><para>
#: debian/xml-man/en/camomilecharmap.xml:72 debian/xml-man/en/camomilelocaledef.xml:77
msgid ""
"This manual page was written for the &debian; distribution because the "
"original program does not have a manual page."
msgstr ""
"Este manual foi escrito para a distribuição &debian; porque o programa "
"original não tem um manual."

#. type: Content of: <refentry><refsect1><para>
#: debian/xml-man/en/camomilecharmap.xml:75
msgid ""
"&dhprg; is made to convert standard charmap file into "
"<filename>.mar</filename> file. This convert a standard charmap file into "
"the internal database format of &camomile;."
msgstr ""
"&dhprg; foi feito para converter um ficheiro charmap standard num ficheiro "
"<filename>.mar</filename>. Isto converte um ficheiro charmap standard "
"para o formato de base de dados interno do &camomile;."

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: debian/xml-man/en/camomilecharmap.xml:82
msgid "<option>-d <parameter>dir</parameter> </option>"
msgstr "<option>-d <parameter>directório</parameter> </option>"

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: debian/xml-man/en/camomilecharmap.xml:87
msgid "Output directory."
msgstr "Directório de saída."

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: debian/xml-man/en/camomilecharmap.xml:93 debian/xml-man/en/camomilelocaledef.xml:109
msgid "<option>-help</option>"
msgstr "<option>-help</option>"

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: debian/xml-man/en/camomilecharmap.xml:96 debian/xml-man/en/camomilelocaledef.xml:112
msgid "<option>--help</option>"
msgstr "<option>--help</option>"

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: debian/xml-man/en/camomilecharmap.xml:99 debian/xml-man/en/camomilelocaledef.xml:115
msgid "Display help message."
msgstr "Mostra mensagem de ajuda."

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: debian/xml-man/en/camomilecharmap.xml:105
msgid "file"
msgstr "ficheiro"

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: debian/xml-man/en/camomilecharmap.xml:108
msgid "Charmap file."
msgstr "Ficheiro charmap."

#. type: Content of: <refentry><refsect1><title>
#: debian/xml-man/en/camomilecharmap.xml:116 debian/xml-man/en/camomilelocaledef.xml:132
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Content of: <refentry><refsect1><para>
#: debian/xml-man/en/camomilecharmap.xml:119
msgid ""
"<citerefentry> <refentrytitle>camomilelocaledef</refentrytitle> "
"<manvolnum>1</manvolnum> </citerefentry>"
msgstr ""
"<citerefentry> <refentrytitle>camomilelocaledef</refentrytitle> "
"<manvolnum>1</manvolnum> </citerefentry>"

#. type: Content of the dhprg entity
#: debian/xml-man/en/camomilelocaledef.xml:5
msgid "<command>camomilelocaledef</command>"
msgstr "<command>camomilelocaledef</command>"

#. type: Content of: <refentry><refmeta><refentrytitle>
#: debian/xml-man/en/camomilelocaledef.xml:39
msgid "CAMOMILELOCALEDEF"
msgstr "CAMOMILELOCALEDEF"

#. type: Content of: <refentry><refnamediv><refpurpose>
#: debian/xml-man/en/camomilelocaledef.xml:46
msgid "a localedef database translators for &camomile;."
msgstr "um tradutor de base de dados localedef para o &camomile;."

#. type: Content of: <refentry><refsynopsisdiv><cmdsynopsis>
#: debian/xml-man/en/camomilelocaledef.xml:51
msgid ""
"&dhprg; <arg choice=\"opt\">--enc <arg choice=\"req\"> "
"<replaceable>encoding</replaceable> </arg> </arg> <arg choice=\"opt\">--file "
"<arg choice=\"req\"> <replaceable>file</replaceable> </arg> </arg> <group> "
"<arg choice=\"plain\">-help</arg> <arg choice=\"plain\">--help</arg> "
"</group> <arg choice=\"opt\"> <replaceable>dir</replaceable> </arg>"
msgstr ""
"&dhprg; <arg choice=\"opt\">--enc <arg choice=\"req\"> "
"<replaceable>encoding</replaceable> </arg> </arg> <arg choice=\"opt\">--file "
"<arg choice=\"req\"> <replaceable>ficheiro</replaceable> </arg> </arg> "
"<group> <arg choice=\"plain\">-help</arg> <arg choice=\"plain\">--help</arg> "
"</group> <arg choice=\"opt\"> <replaceable>directório</replaceable> </arg>"

#. type: Content of: <refentry><refsect1><para>
#: debian/xml-man/en/camomilelocaledef.xml:80
msgid ""
"&dhprg; is made to convert standard localedef file into "
"<filename>.mar</filename> files. This convert and split a standard localedef "
"file into the internal database format of &camomile;."
msgstr ""
"&dhprg; foi feito para converter um ficheiro localedef standard em ficheiros "
"<filename>.mar</filename>. Isto converte e divide um ficheiro localedef "
"standard para o formato de base de dados interno do &camomile;."

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: debian/xml-man/en/camomilelocaledef.xml:87
msgid "<option>--enc <parameter>encoding</parameter> </option>"
msgstr "<option>--enc <parameter>encoding</parameter> </option>"

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: debian/xml-man/en/camomilelocaledef.xml:92
msgid "Encoding used for reading the input file. If omitted, default to UTF-8."
msgstr ""
"Codificação usada para ler o ficheiro de entrada. Se omitido, usa a "
"predefinição UTF-8."

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: debian/xml-man/en/camomilelocaledef.xml:98
msgid "<option>--file <parameter>file</parameter> </option>"
msgstr "<option>--file <parameter>ficheiro</parameter> </option>"

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: debian/xml-man/en/camomilelocaledef.xml:103
msgid "Input localedef file. If ommited, use <filename>stdin</filename>."
msgstr ""
"Insere o ficheiro localedef. Se omitido, usa <filename>stdin</filename>."

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><term>
#: debian/xml-man/en/camomilelocaledef.xml:121
msgid "dir"
msgstr "directório"

#. type: Content of: <refentry><refsect1><variablelist><varlistentry><listitem><para>
#: debian/xml-man/en/camomilelocaledef.xml:124
msgid "Output directory. If omitted, default to the current directory."
msgstr ""
"Directório de saída (gravação de resultados). Se omitido, usa o directório "
"actual por predefinição."

#. type: Content of: <refentry><refsect1><para>
#: debian/xml-man/en/camomilelocaledef.xml:135
msgid ""
"<citerefentry> <refentrytitle>camomilecharmap</refentrytitle> "
"<manvolnum>1</manvolnum> </citerefentry>"
msgstr ""
"<citerefentry> <refentrytitle>camomilecharmap</refentrytitle> "
"<manvolnum>1</manvolnum> </citerefentry>"

#. type: Content of: <refsect1><title>
#: debian/xml-man/en/license.xml:32
msgid "LICENSE"
msgstr "LICENÇA"

#. type: Content of: <refsect1><para>
#: debian/xml-man/en/license.xml:35
msgid ""
"This manual page was written by <personname> <firstname>Sylvain</firstname> "
"<surname>Le Gall</surname> </personname> <email>gildor@debian.org</email> "
"for the Debian GNU/Linux system (but may be used by others).  Permission is "
"granted to copy, distribute and/or modify this document under the terms of "
"the <acronym>GNU</acronym> Lesser General Public License, Version 2.1 or any "
"later version published by the Free Software Foundation; considering as "
"source code all the file that enable the production of this manpage."
msgstr ""
"Este manual foi escrito por <personname> <firstname>Sylvain</firstname> "
"<surname>Le Gall</surname> </personname> <email>gildor@debian.org</email> "
"para o sistema Debian GNU/Linux (mas pode ser usado por outros). É concedida "
"permissão para copiar, distribuir e/ou modificar este documento sob os "
"termos da <acronym>GNU</acronym> Lesser General Public License, Versão 2.1 "
"ou qualquer versão posterior publicada pela Free Software Foundation; "
"considerando como código fonte todos os ficheiros que permitem a "
"produção deste manual."

#. **********************************************************************
#.  refentryinfo.xml
#.
#.  Copyright (C) 2006 Sylvain Le Gall <gildor@debian.org>
#.
#.
#.  This library is free software; you can redistribute it and/or
#.  modify it under the terms of the GNU Lesser General Public
#.  License as published by the Free Software Foundation; either
#.  version 2.1 of the License, or (at your option) any later version;
#.  with the OCaml static compilation exception.
#.
#.  This library is distributed in the hope that it will be useful,
#.  but WITHOUT ANY WARRANTY; without even the implied warranty of
#.  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#.  Lesser General Public License for more details.
#.
#.  You should have received a copy of the GNU Lesser General Public
#.  License along with this library; if not, write to the Free Software
#.  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
#.  MA 02110-1301, USA.
#.
#.  Contact: gildor@debian.org
#. **********************************************************************
#. type: Content of: <refentryinfo>
#: debian/xml-man/en/refentryinfo.xml:31
msgid ""
"  <author> <firstname>Sylvain</firstname> <surname>Le Gall</surname> "
"<email>gildor@debian.org</email> </author> <copyright> <year>2006</year> "
"<holder>Sylvain Le Gall</holder> </copyright> <date>Oct 23, 2006</date> "
"<corpname>Debian</corpname>"
msgstr ""
"  <author> <firstname>Sylvain</firstname> <surname>Le Gall</surname> "
"<email>gildor@debian.org</email> </author> <copyright> <year>2006</year> "
"<holder>Sylvain Le Gall</holder> </copyright> <date>Oct 23, 2006</date> "
"<corpname>Debian</corpname>"

#. type: Content of: outside any tag (error?)
#: debian/xml-man/en/refentryinfo.xml:30
msgid "<placeholder type=\"refentryinfo\" id=\"0\"/>"
msgstr "<placeholder type=\"refentryinfo\" id=\"0\"/>"


