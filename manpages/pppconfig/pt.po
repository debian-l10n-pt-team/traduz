# Translation of pppconfig's manpage to European Portuguese
# Copyright (C) 2014 Free Software Foundation, Inc.
# This file is distributed under the same license as the pppconfig package.
#
# Américo Monteiro <a_monteiro@gmx.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: pppconfig 2.3.21\n"
"POT-Creation-Date: 2011-06-07 20:27+0300\n"
"PO-Revision-Date: 2014-08-24 13:43+0100\n"
"Last-Translator: Américo Monteiro <a_monteiro@gmx.com>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 1.4\n"

#. type: TH
#: man/pppconfig.8:2
#, no-wrap
msgid "PPPCONFIG"
msgstr "PPPCONFIG"

#. type: TH
#: man/pppconfig.8:2
#, no-wrap
msgid "Version 2.3.16"
msgstr "Versão 2.3.16"

#. type: TH
#: man/pppconfig.8:2
#, no-wrap
msgid "Debian GNU/Linux"
msgstr "Debian GNU/Linux"

#. type: SH
#: man/pppconfig.8:3
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: man/pppconfig.8:5
msgid "pppconfig - configure pppd to connect to the Internet"
msgstr "pppconfig - configura o pppd para ligação à Internet."

#. type: SH
#: man/pppconfig.8:5
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: man/pppconfig.8:8
msgid ""
"B<pppconfig> [--version] | [--help] | [[--dialog] | [--whiptail] | "
"[--gdialog] [--noname] | [providername]]"
msgstr ""
"B<pppconfig> [--version] | [--help] | [[--dialog] | [--whiptail] | "
"[--gdialog] [--noname] | [nome-do-provedor]]"

#. type: SH
#: man/pppconfig.8:9
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: Plain text
#: man/pppconfig.8:22
msgid ""
"B<pppconfig> is a B<dialog> based interactive, menu driven utility to help "
"automate setting up a dial out ppp connection.  It provides extensive "
"explanations at each step.  pppconfig supports PAP, CHAP, and chat methods "
"of authentication.  It uses the standard ppp configuration files and sets "
"ppp up so that the standard pon and poff commands can be used to control "
"ppp.  Some features supported by pppconfig are:"
msgstr ""
"B<pppconfig> é um utilitário de menus interactivo baseado em B<dialog> "
"para ajudar a automatizar a configuração de uma ligação ppp de marcação. "
"Disponibiliza explicações extensivas para cada passo. O pppconfig suporta "
"os métodos de autenticação PAP, CHAP e chat. Usa os ficheiros standard "
"de configuração ppp e define o ppp de maneira que os comandos standard "
"pon e poff possam ser usados para controlar o ppp. Algumas características "
"suportadas pelo pppconfig são:"

#. type: Plain text
#: man/pppconfig.8:24
msgid "- Multiple ISP's with separate nameservers."
msgstr " - Múltiplos ISPs com nomes de servidores separados."

#. type: Plain text
#: man/pppconfig.8:26
msgid "- Modem detection."
msgstr " - Detecção de modem."

#. type: Plain text
#: man/pppconfig.8:28
msgid "- Dynamic DNS."
msgstr "- DNS Dinâmico"

#. type: Plain text
#: man/pppconfig.8:30
msgid "- Dial on demand."
msgstr " - Marcação a pedido."

#. type: Plain text
#: man/pppconfig.8:32
msgid "- Allow non-root users to run ppp."
msgstr " - Permite aos utilizadores não-root correr o ppp."

#. type: Plain text
#: man/pppconfig.8:34
msgid "- Uses the gdialog GUI dialog replacement if possible."
msgstr "- Usa a GUI gdialog como substituto do dialog se possível."

#. type: Plain text
#: man/pppconfig.8:44
msgid ""
"Before running pppconfig you should know what sort of authentication your "
"isp requires, the username and password that they want you to use, and the "
"phone number.  If they require you to use chat authentication, you will also "
"need to know the login and password prompts and any other prompts and "
"responses required for login.  If you can't get this information from your "
"isp you could try dialing in with minicom and working through the procedure "
"until you get the garbage that indicates that ppp has started on the other "
"end."
msgstr ""
"Antes de correr o pppconfig você deve saber que tipo de autenticação o "
"seu isp requer, o nome de utilizador e palavra passe que eles querem que "
"você use, e o número de telefone. Se eles quiserem que você use "
"autenticação chat, você também precisa de saber os avisos de utilizador "
"e palavra passe e quaisquer outros avisos e respostas necessárias para "
"autenticação. Se você não consegue esta informação do seu isp você pode "
"tentar ligar com o minicom e trabalhar através do processo até conseguir "
"indicação do outro lado de que o ppp foi iniciado."

#. type: Plain text
#: man/pppconfig.8:52
msgid ""
"B<pppconfig> allows you to configure connections to multiple providers.  For "
"example, you might call your isp 'provider', your employer 'theoffice' and "
"your university 'theschool'.  Then you can connect to your isp with 'pon', "
"your office with 'pon theoffice', and your university with 'pon theschool'."
msgstr ""
"O B<pppconfig> permite-lhe configurar ligações para vários provedores. "
"Por exemplo, você pode chamar ao seu isp \"provedor\", no seu trabalho "
"\"escritório\" e na sua universidade \"escola\". Depois você pode ligar ao "
"seu isp com 'pon', ao seu trabalho com 'pon escritório', e à sua "
"universidade com 'pon escola'."

#. type: Plain text
#: man/pppconfig.8:55
msgid ""
"It can determine which serial port your modem is on, but the serial port "
"must already be configured.  This is normally done when installing Linux."
msgstr ""
"Consegue determinar em qual porta série o seu modem está ligado, mas a "
"porta série tem que já estar configurada. Isto normalmente é feito quando "
"se instala o Linux."

#. type: Plain text
#: man/pppconfig.8:58
msgid ""
"It can help you set your nameservers, or, if your ISP uses 'dynamic DNS', it "
"can set up ppp to use that."
msgstr ""
"Pode ajudá-lo a configurar os seus nomes de servidores, ou se o seu ISP "
"usar 'DNS dinâmico', pode configurar o ppp para o usar."

#. type: Plain text
#: man/pppconfig.8:63
msgid ""
"It can configure ppp for demand dialing, so that your ppp connection will "
"come up automatically.  It will not, however, start pppd for you.  You must "
"still start pppd yourself ('pon' will do it).  Pppd will then wait in the "
"background for you to attempt to access the Net and bring up the link."
msgstr ""
"Pode configurar o ppp para marcação a pedido, para que a sua ligação ppp "
"arranque automaticamente. No entanto, não irá arrancar o pppd para si. "
"Você tem sempre que arrancar o pppd (o 'pon' não o vai fazer). O pppd "
"irá depois esperar em segundo plano por tentativa de aceder à Rede "
"e activar a ligação."

#. type: Plain text
#: man/pppconfig.8:75
msgid ""
"If you select \"Static\" in the \"Configure Nameservers\" screen pppconfig "
"will create a file in the /etc/ppp/resolv directory named after the provider "
"you are configuring and containing \"nameserver\" lines for each of the IP "
"numbers you gave.  This file will be substituted for /etc/resolv.conf when "
"the connection comes up.  The provider name is passed in the ipparam "
"variable so that 0dns-up knows which file to use.  The original resolv.conf "
"will be put back when the connection goes down.  You can edit this file if "
"you wish and add such things as \"search\" or \"domain\" directives or "
"additional nameservers.  Be sure and read the resolv.conf man page first, "
"though.  The \"search\" and \"domain\" directives probably do not do what "
"you think they do."
msgstr ""
"Se você seleccionar \"Estático\" no ecrã \"Configurar Servidores de Nomes\", "
"o pppconfig irá criar um ficheiro no directório /etc/ppp/resolv com o nome "
"do provedor que está a configurar e que contém linhas \"nameserver\" para "
"cada uma dos números IP que você fornecer. Este ficheiro será substituto "
"do /etc/resolv.conf quando a ligação for estabelecida. O nome do provedor "
"é passado na variável ipparam para que o 0dns-up saiba qual ficheiro usar. "
"O resolv.conf original será reposto quando a ligação terminar. Você pode "
"editar este ficheiro se desejar e adicionar coisas como as directivas "
"\"search\" ou \"domain\" ou servidores de nomes adicionais. Certifique-se,"
"no entanto, de ler primeiro o manual do resolv.conf. As directivas "
"\"search\" e \"domain\" provavelmente não fazem o que você pensa que "
"fazem."

#. type: Plain text
#: man/pppconfig.8:83
msgid ""
"If you select \"dynamic\" in the \"Configure Nameservers\" screen pppconfig "
"will configure pppd for 'dynamic DNS' and create a file in the "
"/etc/ppp/resolv directory named after the provider you are configuring but "
"containing nothing.  When the connection comes up the nameservers supplied "
"by your ISP will be added and the file substituted for /etc/resolv.conf.  "
"You can edit this file if you wish and add such things as \"search\" or "
"\"domain\" directives or additional nameservers."
msgstr ""
"Se você seleccionar \"Dinâmico\" no ecrã \"Configurar Servidores de Nomes\", "
"o pppconfig irá configurar o pppd para 'DNS dinâmico' e criar um ficheiro "
"no directório /etc/ppp/resolv com o nome do provedor que está a configurar, "
"mas contendo nada. Quando a ligação ficar activa os servidores de nomes "
"fornecidos pelo seu ISP serão adicionados e o ficheiro um substituto para "
"o /etc/resolv.conf. Você pode editar este ficheiro se desejar e adicionar "
"coisas como as directivas \"search\" ou \"domain\" ou servidores de nomes "
"adicionais."

#. type: Plain text
#: man/pppconfig.8:88
msgid ""
"If you select \"None\" in the \"Configure Nameservers\" screen pppconfig "
"will create no file in /etc/ppp/resolv and will leave /etc/resolv.conf "
"alone. ipparam is not set to the provider name and so is free for the "
"administrator to use."
msgstr ""
"Se você seleccionar \"Nenhum\" no ecrã \"Configurar Servidores de Nomes\", o "
"pppconfig irá criar um ficheiro em /etc/ppp/resolv e não fará nada ao "
"/etc/resolv.conf. O ipparam não é definido para o nome do provedor e "
"por isso fica livre para o administrador usar."

#. type: SH
#: man/pppconfig.8:89
#, no-wrap
msgid "FILES"
msgstr "FICHEIROS"

#. type: Plain text
#: man/pppconfig.8:92
msgid ""
"B</etc/ppp/peers/provider> is the standard pppd options file for the default "
"service provider."
msgstr ""
"B</etc/ppp/peers/provider> é o ficheiro standard de opções do pppd para o "
"provedor de serviço predefinido."

#. type: Plain text
#: man/pppconfig.8:95
msgid ""
"B</etc/ppp/peers/E<lt>nameE<gt>> is the pppd options file for the provider "
"that you have named E<lt>nameE<gt>."
msgstr ""
"B</etc/ppp/peers/E<lt>nomeE<gt>> é o ficheiro de opções do pppd para o "
"provedor que você nomeou de E<lt>nomeE<gt>."

#. type: Plain text
#: man/pppconfig.8:98
msgid ""
"B</etc/ppp/peers/provider.bak> is a backup copy of /etc/ppp/peers/provider."
msgstr ""
"B</etc/ppp/peers/provider.bak> é uma cópia de salvaguarda de "
"/etc/ppp/peers/provider."

#. type: Plain text
#: man/pppconfig.8:101
msgid ""
"B</etc/chatscripts/provider> is the standard chat script for the default "
"service provider."
msgstr ""
"B</etc/chatscripts/provider> é o script standard de chat para o provedor "
"de serviço predefinido."

#. type: Plain text
#: man/pppconfig.8:104
msgid ""
"B</etc/chatscripts/E<lt>nameE<gt>> is the chat script for the provider that "
"you have named E<lt>nameE<gt>."
msgstr ""
"B</etc/chatscripts/E<lt>nomeE<gt>> é o script de chat para o provedor que "
"você nomeou de E<lt>nomeE<gt>."

#. type: Plain text
#: man/pppconfig.8:107
msgid ""
"B</etc/chatscripts/provider.bak> is a backup copy of "
"/etc/chatscripts/provider."
msgstr ""
"B</etc/chatscripts/provider.bak> é uma cópia de salvaguarda de "
"/etc/chatscripts/provider."

#. type: Plain text
#: man/pppconfig.8:110
msgid ""
"B</etc/ppp/resolv> is a directory where resolv.conf files for each provider "
"are stored."
msgstr ""
"B</etc/ppp/resolv> é um directório onde são armazenados os ficheiros "
"resolv.conf de cada provedor de serviço."

#. type: Plain text
#: man/pppconfig.8:114
msgid ""
"B</etc/ppp/ip-up.d/0dns-up> is a script that arranges for the correct "
"resolv.conf file to be copied into place when a connection comes up."
msgstr ""
"B</etc/ppp/ip-up.d/0dns-up> é um script que trata de copiar o ficheiro "
"resolv.conf correcto para o lugar dele quando uma ligação é activada."

#. type: Plain text
#: man/pppconfig.8:118
msgid ""
"B</etc/ppp/ip-down.d/0dns-down> is a script that arranges for the original "
"resolv.conf file to be copied into place when a connection goes down."
msgstr ""
"B</etc/ppp/ip-down.d/0dns-down> é um script que trata de copiar o ficheiro "
"resolv.conf original para o lugar dele quando uma ligação termina."

#. type: Plain text
#: man/pppconfig.8:122
msgid ""
"B</etc/init.d/dns-clean> is a script that runs 0dns-down at bootup to clean "
"up any mess left by a crash."
msgstr ""
"B</etc/init.d/dns-clean> é um script que corre 0dns-down no arranque do "
"sistema para limpar quaisquer lixos deixados por um terminar em erro "
"(crash)."

#. type: Plain text
#: man/pppconfig.8:125
msgid ""
"B</var/run/pppconfig> is a directory where temporary files created by "
"0dns-up are stored."
msgstr ""
"B</var/run/pppconfig> é um directório onde são armazenados os ficheiros "
"temporários criados pelo 0dns-up."

#. type: Plain text
#: man/pppconfig.8:129
msgid ""
"B</var/run/pppconfig/resolv.conf.bak.E<lt>providerE<gt>> is a backup copy of "
"the original resolv.conf file.  0dns-down restores /etc/resolv.conf from it."
msgstr ""
"B</var/run/pppconfig/resolv.conf.bak.E<lt>provedorE<gt>> é uma cópia de "
"salvaguarda do ficheiro resolv.conf original. O 0dns-down restaura o "
"/etc/resolv.conf a partir dele."

#. type: Plain text
#: man/pppconfig.8:134
msgid ""
"B</var/run/pppconfig/0dns.E<lt>providerE<gt>> is a backup copy of the "
"resolv.conf file for E<lt>providerE<gt>.  0dns-down uses it to determine if "
"/etc/resolv.conf has been overwritten by another process."
msgstr ""
"B</var/run/pppconfig/0dns.E<lt>provedorE<gt>> é uma cópia de salvaguarda "
"do ficheiro resolv.conf para o E<lt>provedorE<gt>. O 0dns-down usa-o para "
"determinar se o /etc/resolv.conf foi sobrescrito por outro processo."

#. type: Plain text
#: man/pppconfig.8:141
msgid ""
"B</etc/ppp/pap-secrets> and B</etc/ppp/chap-secrets> are described in the "
"pppd documentation.  pppconfig may add lines to these files and will change "
"lines that it previously added."
msgstr ""
"B</etc/ppp/pap-secrets> e B</etc/ppp/chap-secrets> estão descritos na "
"documentação do pppd. O pppconfig pode adicionar linhas a estes ficheiros "
"e pode mudar linhas que adicionou previamente."

#. type: SH
#: man/pppconfig.8:141
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: man/pppconfig.8:144
msgid "B<pppconfig> requires pppd 2.3.7 or higher."
msgstr "B<pppconfig> requer pppd 2.3.7 ou superior."

#. type: SH
#: man/pppconfig.8:144
#, no-wrap
msgid "TO DO"
msgstr "A FAZER"

#. type: Plain text
#: man/pppconfig.8:146
msgid "Add full support for MSCHAP."
msgstr "Adicionar suporte completo para MSCHAP."

#. type: SH
#: man/pppconfig.8:146
#, no-wrap
msgid "BUGS"
msgstr "BUGS"

#. type: Plain text
#: man/pppconfig.8:148
msgid "Don't tell pppconfig to find your modem while pppd is running."
msgstr ""
"Não diga ao pppconfig para encontrar o seu modem enquanto o pppd está "
"a correr."

#. type: SH
#: man/pppconfig.8:148
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: man/pppconfig.8:152
msgid ""
"B<chat(8), gpppon(1), plog(1), poff(1), pon(1), pppd(8),> and "
"B<whiptail(1).>"
msgstr ""
"B<chat(8), gpppon(1), plog(1), poff(1), pon(1), pppd(8),> e "
"B<whiptail(1).>"

#. type: SH
#: man/pppconfig.8:152
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: man/pppconfig.8:155
msgid "B<pppconfig> was written by John Hasler E<lt>jhasler@debian.orgE<gt>."
msgstr "B<pppconfig> foi escrito por John Hasler E<lt>jhasler@debian.orgE<gt>."

#. type: SH
#: man/pppconfig.8:155
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: man/pppconfig.8:158
msgid ""
"B<This man page may be treated as if it were> B<in the public domain. I "
"waive all rights.>"
msgstr ""
"B<This man page may be treated as if it were> B<in the public domain. I "
"waive all rights.>"


